<?php


class CashfreeBeneficiary_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'cashfree_beneficiary';
    }
    
    public function getRecords($userUniqueId){
        if(empty($userUniqueId))
            return false;
        $query = $this->db->where('user_unique_id', $userUniqueId)->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
}