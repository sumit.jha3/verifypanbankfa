<?php


class FaUserPanBank_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'fa_user_pan_bank';
    }
    
    public function getUserDetail($user_unique_id){
        if(empty($user_unique_id))
            return false;
        $result = false;
        $query = $this->db->where('user_unique_id', $user_unique_id)->where('deleted', 0)->get($this->table_name, 1);
        $array = $query->result_array();
        if(!empty($array))
            $result = $array[0]; 
        return $result;
    }
    
       
    public function getAllRecord($data){
        $offset = isset($data['offset'])? $data['offset']: 0;
        $size = isset($data['size'])? $data['size']: 50;
        
        $this->db->from($this->table_name);
        if(!empty($data) && isset($data['totalCount'])){
            return $this->db->where('deleted', 0)->count_all_results();
        }
        
        if(!empty($data) && isset($data['bank_reprocess'])){
            $this->db->where('pan_verified', 1)->where('bank_verified', 0)->where('extra_data is NOT NULL', NULL, FALSE);
        }
        if(!empty($data) && isset($data['ids'])){
            $this->db->where_in('id', $data['ids']);
        }
        
        $this->db->where('deleted', 0)->order_by('id', 'DESC');
        $query = $this->db->limit($size, $offset)->get(); 
        return $query->result_array();
    }
    
    public function bulInsert($bulkInsert){
        if(!empty($bulkInsert) && is_array($bulkInsert)){
            $query = $this->db->insert_batch($this->table_name, $bulkInsert);
            return $query;
        }
    }
    
    public function insert($data){
        if(!empty($data) && is_array($data)){
            $query = $this->db->insert($this->table_name, $data);
            return $query;
        }
    }
    
    public function updateDocument($update, $condition){
        return $this->db->update($this->table_name, $update, $condition);
    }

}