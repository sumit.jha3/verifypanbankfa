<?php


class ReferralAmountDistribution_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'referral_amount_distribution';
    }
    
    public function getUserReferralDetail($referred_user_id, $user_unique_id){
        if(empty($user_unique_id) || empty($referred_user_id))
            return false;
        
        $query = $this->db->where('user_unique_id', $user_unique_id)->where('referred_user_id', $referred_user_id)->get($this->table_name, 1);
        $array = $query->result_array();
        return (!empty($array))? $array[0]: [];
    }
    
    
    public function insert($data){
        if(!empty($data) && is_array($data)){
            $query = $this->db->insert($this->table_name, $data);
            return $query;
        }
    }
    
    public function updateDocument($update, $condition){
        return $this->db->update($this->table_name, $update, $condition);
    }

}