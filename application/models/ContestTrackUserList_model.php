<?php


class ContestTrackUserList_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'contest_track_user_list';
    }
    
    public function getUsers(){
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    public function updateDocument($update, $condition){
        return $this->db->update($this->table_name, $update, $condition);
    }
    
    public function getActiveUsers(){
        $query = $this->db->where('status', 1)->get($this->table_name);
        return $query->result_array();
    }
}