<?php


class Employee_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
    }
    
    public function checkEmployeeExists($username, $password){
        if(empty($username) || empty($password))
            return false;
        $query = $this->db->where('username', $username)->where('password', md5($password))->get('employee',1);
        return $query->result_array();
    }

}