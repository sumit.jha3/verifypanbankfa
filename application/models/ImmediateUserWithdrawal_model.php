<?php


class ImmediateUserWithdrawal_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'immediate_user_withdrawal';
    }
    
    public function getUser($userUniqueId){
        if(empty($userUniqueId))
            return false;
        $query = $this->db->where('user_unique_id', $userUniqueId)->where('status',1)->get($this->table_name);
        $users = $query->result_array();
        return !empty($users)? $users[0]: [];
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    public function rawQueryExecute($query){
        return $query = $this->db->query($query);
    }
    
    public function getAllRecords($data){
        $offset = isset($data['offset'])? $data['offset']: 0;
        $size = isset($data['size'])? $data['size']: 20;
        
        $this->db->from($this->table_name);
        if(!empty($data) && isset($data['totalCount'])){
            return $this->db->count_all_results();
        }
        
        if(isset($data['status']) && $data['status'] != ''){
            $this->db->where('status', $status);
        }
        
        $this->db->order_by('id', 'DESC');

        $query = $this->db->limit($size, $offset)->get(); 
        return $query->result_array();
    }
}