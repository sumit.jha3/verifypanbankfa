<?php


class Settings_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'settings';
    }
    
    public function getAllSettings(){
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }
    
    
    public function updateDocument($update, $condition){
        $query = $this->db->update($this->table_name, $update, $condition);
    }
    
    public function getSettingByName($name){
        $query = $this->db->where('setting_name', $name)->get($this->table_name, 1);
        return $query->result_array()[0];
    }
    
    public function getSettingByNameList($name){
        $query = $this->db->where_in('setting_name', $name)->get($this->table_name);
        return $query->result_array();
    }

}