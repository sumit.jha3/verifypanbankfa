<?php


class Withdrawal_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'withdrawal';
    }
    
    public function getRecords($userUniqueId){
        if(empty($userUniqueId))
            return false;
        $query = $this->db->where('user_unique_id', $userUniqueId)->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    public function getAllRecords($page = 1, $size = 50, $status = ''){
        if(!empty($status)){
            $query = $this->db->where('status', strtolower($status))->order_by('created_at', 'DESC')->get($this->table_name, $size);
        }else{
            $query = $this->db->order_by('created_at', 'DESC')->get($this->table_name, $size);
        }
        return $query->result_array();
    }
    
    public function getAllStatusRecords($status, $page = 1, $size = 500){

        if($status == 'success'){
            // means : Transfer Scheduled for next working day.
            $query = $this->db->where('status', $status)->where('status_code', 201)->where('process_block', 0)->order_by('created_at', 'DESC')->get($this->table_name, $size);
            return $query->result_array();
        }else if($status == 'pending'){
            $query = $this->db->where('status', $status)->where('process_block', 0)->order_by('created_at', 'DESC')->get($this->table_name, $size);
            return $query->result_array();
        }
        return false;
        
    }
    
    public function updateDocument($update, $condition){
        $query = $this->db->update($this->table_name, $update, $condition);
    }
    
    public function getOrderDetail($orderId)
    {
        if(empty($orderId))
            return false;
        $query = $this->db->where('transferId', $orderId)->get($this->table_name);
        $record = $query->result_array();
        return !empty($record)?$record[0]: [];
    }
}