<?php


class CricketScorecard_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'cricket_scorecard';
    }
    
    public function getRecords($data = []){
            
        if(!empty($data['match_id']))
             $this->db->where('match_id', $data['match_id']);
               
        $query = $this->db->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    

    
    public function updateDocument($update, $condition){
        return $this->db->update($this->table_name, $update, $condition);
    }
    

}