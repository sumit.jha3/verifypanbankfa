<?php


class TrackUserContest_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'track_users_contest';
    }
    
    public function getUserContestRecord($userId, array $contestIds){
        if(empty($contestIds) || empty($userId))
            return false;
        $query = $this->db->where('user_id', $userId)->where_in('contest_id', $contestIds)->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    public function bulInsert($bulkInsert){
        if(!empty($bulkInsert) && is_array($bulkInsert)){
            $query = $this->db->insert_batch($this->table_name, $bulkInsert);
            return $query;
        }
    }
}