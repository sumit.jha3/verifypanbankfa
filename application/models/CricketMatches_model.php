<?php


class CricketMatches_model extends CI_Model {
    
    public function __construct()
    {
        $this->load->database();
        $this->table_name = 'cricket_matches';
    }
    
    public function getRecords($data = []){
            
        if(!empty($data['match_id']))
             $this->db->where('match_id', $data['match_id']);
        
        if(!empty($data['match_date'])){
            $date = gmdate('Y-m-d H:i:s', strtotime($data['match_date']));
            $this->db->where("date_start <= '".$date."' AND date_end >= '".$date."' ", NULL, FALSE);
        }
        
        if(!empty($data['status_str']))
            $this->db->where_in('status_str', $data['status_str']);
        
        if(isset($data['team_declare']))
            $this->db->where('team_declare', (int) $data['team_declare']);
        
        $query = $this->db->where('status', 1)->get($this->table_name);
        return $query->result_array();
    }
    
    public function insert($data){
        if(empty($data))
            return false;
        $query = $this->db->insert($this->table_name, $data);
        return $query;
    }
    
    
    public function getIncomleteSquadMatches($data = []){
        $query = $this->db->where('team_declare', 0)->where("status_str",'scheduled')->where('status', 1)->where('team_a_squad is NULL', NULL, false)->get($this->table_name);
        return $query->result_array();
    }
    
    public function updateDocument($update, $condition){
        return $this->db->update($this->table_name, $update, $condition);
    }
    
    public function getIncomleteLineupMatches(){
        $time_1_H_added = gmdate('Y-m-d H:i:s', strtotime('+ 5 hours'));
        $query = $this->db->where('team_declare', 0)->where("status_str",'scheduled')->where("date_start <= '".$time_1_H_added."' ", Null , False)->where('status', 1)->get($this->table_name);
        return $query->result_array();
    }
}