<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if(!function_exists("get_pending_user_pan_record")){
    function get_pending_user_pan_record(){
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/users";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $to_date = date_time_in_ist(date('Y-m-d H:i:s'), 'Y-m-d');
        $body = [
            'from_date' => '2022-01-01',
            'to_date' => $to_date,
            'current_page' => 1,
            'status' => 1,
            'pending_pan_approval' => 1,
            'is_flag' => '', 'keyword' => '', 'items_perpage' => 1000, 'sort_field' => 'added_date', 'sort_order' => 'DESC'];
        
            $result = curl_request($url, $header, 'POST', $body);
            return $result;
    }
}


if(!function_exists("updatePanVerifiedApiFA")){
    function updatePanVerifiedApiFA($userUniqueId, $pan_verified = 1, $message = ''){
        // ---------- $pan_verified = 1 means approve, $pan_verified=2 means reject
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/verify_user_pancard";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $body = ['user_unique_id' => $userUniqueId, 'pan_verified' => $pan_verified, 'pan_rejected_reason' => $message];
        $result = curl_request($url, $header, 'POST', $body);
        return $result;
    }
}


if(!function_exists("updatePanInfoApiFA")){
    function updatePanInfoApiFA($body){
        if(empty($body))
            return false;
        
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/user/update_pan_info";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $result = curl_request($url, $header, 'POST', $body);
        return $result;
    }
}


if(!function_exists('ageCalculate')){
    function ageCalculate($dob){
        $dob = new DateTime($dob);
        $today = new DateTime(date('Y-m-d'));
        $result = $today->diff($dob);
        return $result;
    }
}


if(!function_exists("get_user_bank_detail")){
    function get_user_bank_detail($userId){
        if(empty($userId))
            return false;
        
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/get_user_bank_data";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $result = curl_request($url, $header, 'POST', ['user_unique_id' => $userId]);
        // response Result : {"service_name":"get_user_bank_data","message":"","global_error":"","error":[],"data":{"first_name":"bharat","last_name":"","bank_name":"syndicate","ac_number":"33455299763","ifsc_code":"SBIF0987613","micr_code":null,"upi_id":""},"response_code":200}
        return $result;
    }
}


if(!function_exists("update_user_bank_detail")){
    function update_user_bank_detail($body){
        if(empty($body))
            return false;
        
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/user/update_bank_ac_detail";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $result = curl_request($url, $header, 'POST', $body);
        return $result;
    }
}
if(!function_exists("verifyUserBankApiFA")){
    function verifyUserBankApiFA($body){
        if(empty($body))
            return false;
        
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/verify_user_bank";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $result = curl_request($url, $header, 'POST', $body);
        return $result;
    }
}


?>