<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('getReferralUsers')){
    function getReferralUsers($forUserId, $size = 100, $page = 1, $start = "2019-08-24", $end = ''){
        if(empty($forUserId))
            return [];
        
        $end = empty($end)? date('Y-m-d'): $end;
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/get_user_referral_data";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $body = ['user_id' => $forUserId, 'from_date' => $start, 'to_date' => $end, 'items_perpage' => $size, 'current_page' => $page];
        $result = curl_request($url, $header, 'POST', $body);
        if(!empty($result) && !empty($result['data']) && isset($result['data']['referral_trend'])){
            unset($result['data']['referral_trend']);
        }
        return $result;
        
    }
}


if(!function_exists('getUserDetail')){
    function getUserDetail($userUniqueId){
        if(empty($userUniqueId))
            return [];
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/get_user_detail";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $body = ['user_unique_id' => $userUniqueId];
        return curl_request($url, $header, 'POST', $body);
    }
}



if(!function_exists('addUserBalance')){
    function addUserBalance($body){
        if(empty($body))
            return [];
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/user/add_user_balance";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];

        return curl_request($url, $header, 'POST', $body);
    }
}