<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists("ci")){
    function ci(){
        $CI =& get_instance();
        return $CI;
    }
    
}

if(!function_exists("is_logged_in")){
    function is_logged_in(){
        $CI =& get_instance();
        return $CI->session->has_userdata('user_session');
    }
}


if(!function_exists("redirect_url")){
    function redirect_url($path){
        if(empty($path))
            return false;
        
        $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') ? "https://": "http://";   
        
        $url = $url.$_SERVER['HTTP_HOST']."/" . rtrim(ltrim($path, "/"), "/") ;
        header('Location: '. $url, TRUE, 200);
    }
}


if(!function_exists("curl_request")){
    function curl_request($baseUri, $header , $method = 'GET', $body = ''){
//        print_r(func_get_args());
        if(empty($header))
            $header[] = 'Content-Type:application/json';
        

        $ci = curl_init();
        curl_setopt($ci, CURLOPT_URL, $baseUri);
        curl_setopt($ci, CURLOPT_TIMEOUT, 200);
        curl_setopt($ci, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ci, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ci, CURLOPT_FORBID_REUSE, 0);
        curl_setopt($ci, CURLOPT_CUSTOMREQUEST, $method);
       
        if(!empty($body) ){
            if(is_array($body))
                curl_setopt( $ci, CURLOPT_POSTFIELDS, json_encode($body) );
            else 
                curl_setopt( $ci, CURLOPT_POSTFIELDS, $body );
        }
        $response = curl_exec($ci);

        curl_close($ci);
        $res = json_decode($response, true);
        return $res;
    
    }
}

if(!function_exists("is_production_env")){
    function is_production_env(){
        return ENVIRONMENT == 'production' ? true: false;
    }
}

if (!function_exists('dd')) {
 function dd()
  {
      echo '<pre>';
      array_map(function($x) {
          if(is_array($x)){
              print_r($x);
          }
          else if(is_null($x)){
              echo "NULL";
          }else if(is_bool($x)){
              echo var_dump($x);
          }elseif(is_string($x) || is_numeric($x)|| is_int($x)){
              echo $x;
          }
          else{
               print_r($x);
          }
          echo PHP_EOL;
      }, func_get_args());
      echo '</pre>';
      die;
   }
 }
 
 if(!function_exists('time_diff_from_now')){
    function time_diff_from_now($dateTime){
        $requestedDateTime = new DateTime($dateTime, new DateTimeZone('UTC'));
        $requestedDateTime->setTimezone(new DateTimeZone('Asia/Kolkata'));
        
        $currentDateTime = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone('UTC'));
        $currentDateTime->setTimezone(new DateTimeZone('Asia/Kolkata'));

        $diffInSec = strtotime($currentDateTime->format('Y-m-d H:i:s')) - strtotime($requestedDateTime->format('Y-m-d H:i:s'));
        return $diffInSec > 0? $diffInSec: 0;
    }
 }
 
 if(!function_exists('date_time_in_ist')){
     function date_time_in_ist($date = '', $fomat = 'Y-m-d H:i:s'){
        $date = empty($date)? date('Y-m-d H:i:s'): $date;
        $requestedDateTime = new DateTime($date, new DateTimeZone('UTC'));
        $requestedDateTime->setTimezone(new DateTimeZone('Asia/Kolkata'));
        return $requestedDateTime->format($fomat);
     }
 } 
 
 if(!function_exists('pagination')){
     function pagination($base_url, $per_page, $totalCount){
        $ci = ci();
        $ci->load->library('pagination'); 
        $ci->load->helper('url');
        $uriSegment = 3; 
         
        $config['base_url']    = $base_url; 
        $config['uri_segment'] = $uriSegment;
        $config['total_rows']  = $totalCount; 
        $config['per_page']    = $per_page; 
        $config['num_links'] = 8;
        
        $config['reuse_query_string'] = TRUE;
        $config['full_tag_open'] = '<div class="pagination">';
        $config['full_tag_close'] = '</div>';

        $config['first_link'] = 'First Page';
        $config['first_tag_open'] = '<span class="firstlink">';
        $config['first_tag_close'] = '</span>';

        $config['last_link'] = 'Last Page';
        $config['last_tag_open'] = '<span class="lastlink">';
        $config['last_tag_close'] = '</span>';

        $config['next_link'] = '>';
        $config['next_tag_open'] = '<span class="nextlink">';
        $config['next_tag_close'] = '</span>';

        $config['cur_tag_open'] = '<span class="curlink">';
        $config['cur_tag_close'] = '</span>';

        $config['num_tag_open'] = '<span class="numlink">';
        $config['num_tag_close'] = '</span>';
        
        $ci->pagination->initialize($config); 

        $page = $ci->uri->segment($uriSegment); 
        $offset = !$page?0:$page;
        return $offset;
     }
 } 
 
 if(!function_exists('get_all_queries')){
     function get_all_queries(){
        $ci = ci();
        if ( ! class_exists('CI_DB', FALSE))
        {
            class CI_DB extends CI_DB_driver { }
        }
        
        return $ci->db->all_query();
     }
 }
 
 if(!function_exists('send_email')){
    function send_email($to, $subject = "", $message = "", $from_email = 'bharat.bhushan@supersixsports.com', $from_name = 'Bharat Tech Team' )
    {
        $ci = ci();
        /* configuration and sending email */
        $config = array();
        $config['smtp_host'] = 'email-smtp.ap-south-1.amazonaws.com';
        $config['smtp_user'] = 'AKIAWTA4JJVSQJ5M4US2';
        $config['smtp_pass'] = 'BIOE/7mEOopsW1iMB/5m+pakcT4gOXDAmMDooENXkuE5';
        $config['smtp_port'] = 465;
        $config['protocol']  = 'smtp';
        $config['smtp_crypto'] = 'ssl';
        $config['mailpath']  = '';
        $config['mailtype']  = 'html';
        $config['charset']   = 'utf-8';
        $config['wordwrap']  = TRUE;
//dd($config);
        $ci->load->library('email');
        $email = new CI_Email();
        $email->initialize($config);
        $email->set_newline("\r\n");
        $email->clear();
        $email->from($from_email, $from_name);
        $email->to(trim($to));
        $email->subject($subject);

//        $email->reply_to('noreply@supersixsports.com', $from_name);
        $email->message($message);

        if ($email->send()) {
            return TRUE;
        } else {
            //$email->print_debugger(); //for debuging
            return FALSE;
        }
    }
 }
?>