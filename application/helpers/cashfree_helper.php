<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if(!function_exists('cashfree_host')){
    function cashfree_host(){
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $env = is_production_env();
        return $cashfree['host'][$env?'production':'development'];
    }
}

if(!function_exists("fa_get_pending_withdrawal_list")){
    function fa_get_pending_withdrawal_list($keyword = '', $userId = ''){
        // status = 0 means pending
        $to_date = date_time_in_ist(date('Y-m-d H:i:s'), 'Y-m-d');
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/finance/get_all_withdrawal_request";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $body = ['activity_duration' => '', 'csv'=> false, 'current_page'=>1, 'filter_date_type'=>"0", 'from_date'=>"2022-05-01", 'items_perpage'=> 200,
            "keyword"=>$keyword, "sort_field"=>"PWT.created_date", "sort_order"=> "DESC", "status"=> "0", "to_date"=> $to_date, "total_items"=>0, "type"=> "", "withdraw_method"=>"0", "from_panbank_cron" => 1];
        
        if(!empty($userId)){
          $body['user_id'] = $userId;
        }
        $result = curl_request($url, $header, 'POST', $body);
        return $result;
    }
}

if(!function_exists('cashfree_get_bearer_token')){
    function cashfree_get_bearer_token(){
        $host = cashfree_host();
        
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $url = $host."/payout/v1/".$cashfree['authorizeApi'];
        $env = is_production_env();
        if($env){
            $header = $cashfree['header_production'];
        }else{
            $header = $cashfree['header'];
        }
        $response = curl_request($url, $header, 'POST');
        
        if(!empty($response) && ($response['subCode'] == 200) && strtolower($response['status']) == 'success' && !empty($response['data'])){
            return $response['data']['token'];
        }
        
        return '';
    }
}


if(!function_exists('cashfree_getBeneficiary')){
    function cashfree_getBeneficiary($userUniqueId){
        if(empty($userUniqueId))
            return "User Unique Id can not be empty. ";
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $url = $host."/payout/v1/".$cashfree['getBeneficiaryApi']."/$userUniqueId";
        $header = ['Authorization: Bearer '.$bearerToken, 'cache-control: no-cache', 'content-type:application/json'];
        $response = curl_request($url, $header, 'GET');
        return $response;
    }
}



if(!function_exists('cashfree_addBeneficiary')){
    function cashfree_addBeneficiary($body){
        if(empty($body))
            return false;
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $url = $host."/payout/v1/".$cashfree['addBeneficiaryApi'];
        $header = ['Authorization: Bearer '.$bearerToken, 'cache-control: no-cache', 'content-type:application/json'];
        $response = curl_request($url, $header, 'POST', $body);
        return $response;
    }
}




if(!function_exists('cashfree_requestTransfer')){
    function cashfree_requestTransfer($body){
        if(empty($body))
            return false;
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $url = $host."/payout/v1/".$cashfree['requestTransferApi'];
        $header = ['Authorization: Bearer '.$bearerToken, 'content-type:application/json'];
        $response = curl_request($url, $header, 'POST', $body);
        return $response;
    }
}


if(!function_exists('cashfree_removeBeneficiary')){
    function cashfree_removeBeneficiary($beneficiary){
        if(empty($beneficiary))
            return "Beneficiary Id can not be empty. ";
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        
        $ci = ci();
        $cashfree = $ci->config->item('cashfree');
        $url = $host."/payout/v1/removeBeneficiary";
        $header = ['Authorization: Bearer '.$bearerToken, 'content-type:application/json'];
        $response = curl_request($url, $header, 'POST', ['beneId' => $beneficiary]);
        return $response;
    }
}


if(!function_exists('cashfree_getBalance')){
    function cashfree_getBalance(){
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        
        $ci = ci();
        $url = $host."/payout/v1/getBalance";
        $header = ['Authorization: Bearer '.$bearerToken];
        $response = curl_request($url, $header, 'GET');
        if(!empty($response) && $response['subCode'] == 200 && !empty($response['data']) && !empty($response['data']['availableBalance'])){
            return $response['data']['availableBalance'];
        }
        else{
            // send email to Team
        }
        return 0;
    }
}
if(!function_exists('cashfree_getBeneId')){
    function cashfree_getBeneId($account, $ifsc){
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
        $ci = ci();
        $url = $host."/payout/v1/getBeneId?bankAccount=$account&ifsc=$ifsc";
        $header = ['Authorization: Bearer '.$bearerToken];
        $response = curl_request($url, $header, 'GET');
        
        return $response;
    }
}

if(!function_exists('cashfree_getTransferStatus')){
    function cashfree_getTransferStatus($transferId = '', $referenceId = ''){
        $string = (!empty($transferId))? "transferId=".$transferId : (!empty($referenceId)? "referenceId=".$referenceId: '');        
        if(empty($string)){
            echo "Empty transferId/referenceId";
            return false;
        }
        
        $bearerToken = cashfree_get_bearer_token();
        if(empty($bearerToken)){
            echo "Invalid Bearer Token";
            return false;
        }
        
        $host = cashfree_host();
//        $url = $host."/payout/v1.1/getTransferStatus?".$string;
        $url = $host."/payout/v1/getTransferStatus?".$string;
        $header = ['Authorization: Bearer '.$bearerToken, 'Content-Type: application/json'];
        $response = curl_request($url, $header, 'GET');
        
        return $response;
    }
}


if(!function_exists('cashfree_reprocess_withdrawal')){
    function cashfree_reprocess_withdrawal($status){
        if(!in_array($status, ['pending', 'success'])){
            return false;
        }
        $ci = ci();
        $ci->load->database();
        $ci->load->model('CashfreeBeneficiary_model');
        $ci->load->model('Withdrawal_model');
        
        $withdrawals  = $ci->Withdrawal_model->getAllStatusRecords($status);
        if(!empty($withdrawals)){
            foreach($withdrawals as $withdrawalDetail){
               
                $updateFa = false;
                $reprocessTransfer = false;
                $response = cashfree_getTransferStatus($withdrawalDetail['latest_transferId'], $withdrawalDetail['referenceId']);
                if(!empty($response))
                {
                    if($response['subCode'] == 200 && !empty($response['data']['transfer']) && ($response['data']['transfer']['status'] == 'SUCCESS')){
                        $updateFa = true;
                    }else if ($response['subCode'] == 404){ // in case TransferId or ReferenceId is failed so we will try again for both check
                        // TransferId is invalid or does not exist
                        $response = cashfree_getTransferStatus('', $withdrawalDetail['referenceId']);
                        if($response['subCode'] == 200 && !empty($response['data']['transfer']) && ($response['data']['transfer']['status'] == 'SUCCESS')){
                            $updateFa = true;
                        }else if ($response['subCode'] == 404){ // ReferenceId is invalid or does not exist
                            $response = cashfree_getTransferStatus($withdrawalDetail['latest_transferId'], '');
                            if($response['subCode'] == 200 && !empty($response['data']['transfer']) && ($response['data']['transfer']['status'] == 'SUCCESS')){
                                $updateFa = true;
                            }
                        }
                    }else{
                        if($response['subCode'] == 200 &&  !empty($response['data']['transfer']) && ($response['data']['transfer']['status'] == 'REVERSED')){
                            $transfer = $response['data']['transfer'];
                            if(in_array($transfer['reason'], ['ANY_OTHER_REASON', 'BENEFICIARY_BANK_NODE_OFFLINE'])){
                                // reprocess Withdrawal 
                                $orderId = $withdrawalDetail['latest_transferId'];
                                $expl = explode("_", $orderId);
                                $latest_transferId = isset($expl[1])? $expl[0]."_".($expl[1]+1): $expl[0]."_1";
                                
                                $body = ['user_unique_id' => $withdrawalDetail['user_unique_id'] ,'beneId' => $transfer['beneId'], 'amount' =>(float) $withdrawalDetail['amount'], 'transferId' => $orderId,
                                    'user_name' => $withdrawalDetail['user_name'], 'full_name' => $withdrawalDetail['full_name'], 'bankAccount' => $withdrawalDetail['bankAccount'], 'ifsc' => $withdrawalDetail['ifsc']];
                                requestTransfer($body, $orderId, $latest_transferId);
                                $reprocessTransfer = true;
                                
                            }
                        }
                    }
                }
                // ------ withdrawal table update from requestTransfer() so we will not update below
                if($reprocessTransfer){
                    continue;
                }
                
                $update = [];
                $update['status'] = (in_array($response['data']['transfer']['status'], ['SUCCESS', 'PENDING']))? strtolower($response['data']['transfer']['status']): 'error';
                if($updateFa){
                    // update on FA
                    faChangeWithdrawalStatus($withdrawalDetail['transferId'], 1);
                    $update['process_block'] = 1;
                    $update['processedOn'] = (!empty($response) && !empty($response['data'])  && !empty($response['data']['transfer'])?$response['data']['transfer']['processedOn']  : null);
                }
                
                if(!empty($response['data']['transfer']) &&  $update['status'] == 'success'){
                    $update['status_code'] = 200;
                }
                
                $update['message'] = $response['message'];
                if(!empty($response['data']['transfer']['reason'])){
                    $update['message'] = $response['data']['transfer']['status']." && ". $response['data']['transfer']['reason'];
                }
                // update on Withdrawal_model
                $update['process_attempt'] = $withdrawalDetail['process_attempt'] + 1;
                $ci->Withdrawal_model->updateDocument($update, ['id' => $withdrawalDetail['id']]);
            }
        }
    }
}


if(!function_exists('pendingWithdrawalProcessingHelper'))
{
    function pendingWithdrawalProcessingHelper($for = 'all', $amountLimit = 1000){
        $balance = cashfree_getBalance();
        if(empty($balance))
        {
            echo json_encode(['error' => "Cashfree balance is 0"]);
            return false;
        }
        $ci = ci();
        $ci->load->database();
        $ci->load->model('ImmediateUserWithdrawal_model');
        $ci->load->model('Settings_model');
        
        $config = $ci->config->item('cashfree');
        $time_after_accept = $config['common_withdrawal_time'];
        $setting = $ci->Settings_model->getSettingByName('normal_user_withdrawal_time');
        if(!empty($setting) && $setting['status'] == 1){
            $time_after_accept = $setting['setting_value'];
        }
//        $time_after_accept = (!empty($time_after_accept) && $time_after_accept < 5)? 5 : $time_after_accept;
              
        $setting = $ci->Settings_model->getSettingByName('withdrawal_limit');
        if(!empty($setting)){
            if($setting['status'] == 0){
                echo json_encode(['error' => "Withdrawal will not process because of Withdrawal Status is inactive."]);
                return false;
            }
            $amountLimit = $setting['setting_value'];
        }
        // -------- get all pending withdrawal list from Fantasy Akhada Api
        $pendingList = fa_get_pending_withdrawal_list();

        if(!empty($pendingList) && !empty($pendingList['data']) && !empty($pendingList['data']['result']))
        {
            $all_order = [];
            foreach($pendingList['data']['result'] as $request)
            {
                
                if($balance-$request['winning_amount'] < 1){
                    // send email to team regarding balance update
                    continue;
                }
                
                // -------block user here. is_flag = 1 means that user have mark as a fraud user -----------//
                if($request['user_unique_id'] == '7ea7d50797' || (isset($request['is_flag']) && ($request['is_flag'] == 1)) ){
                    continue;
                }
                
                // -------- process user if user have in "immediate_user_withdrawal" table   OR   have request withdrawal time more than $time_after_accept time.
                $immediateUser = $ci->ImmediateUserWithdrawal_model->getUser($request['user_unique_id']);
                if($for == 'process_only_immediate_user' && empty($immediateUser)){
                    continue;   
                }else if(empty($immediateUser)){
                    $diffInSec = time_diff_from_now($request["order_date_added"]); // retrun in Seconds
                    if(($diffInSec/60) < $time_after_accept){
                         echo PHP_EOL." Time is not less then : $time_after_accept minutes.".PHP_EOL;
                        continue;
                    }
                }
                
                if(empty($immediateUser) && ($for != 'process_only_immediate_user') && ($request['winning_amount'] > $amountLimit)){
                    echo PHP_EOL." Amount is more than our process limit $amountLimit so we will ignore this. We are in testing mode";
                    continue;
                }
                
                // ------ if user have more then 1 pending withdrawal request on Fantasy Akhada then reject user all pending Request.
                if(in_array($request['order_id'], $all_order)){
                   continue; 
                }
                
                if(!empty($immediateUser)){
                    sleep(30);      // in case of any lag then we will hault the script for 30 seconds in case of high roller
                }
                $UserPendingRequest = fa_get_pending_withdrawal_list('', $request['user_id']);
                if(!empty($UserPendingRequest) && !empty($UserPendingRequest['data']) && !empty($UserPendingRequest['data']['result']) && (count($UserPendingRequest['data']['result']) > 1)){
                    $orderIds = [];
                    // get all user pending request for a user
                    foreach ($UserPendingRequest['data']['result'] as $detail){
                        if($detail['user_unique_id'] == $request['user_unique_id']){
                            $all_order[] = $orderIds[] = $detail['order_id'];
                        }
                    }
                    // finally confirm, user have more then 1 pending request at a time. if yes, we will reject all request 
                    if(!empty($orderIds) && count($orderIds) > 1){
                        foreach($orderIds as $orderId){
                            faChangeWithdrawalStatus($orderId, 2, "Multiple withdrawal requested together. Please raise new request.");
                        }
                        
                        
                        continue;
                    }
                }
                
                $beneficiaryDetail = checkBeneficiary($request);
                if(!empty($beneficiaryDetail) && is_array($beneficiaryDetail))
                {
                    $body = ['user_unique_id' => $request['user_unique_id'] ,'beneId' => $beneficiaryDetail['beneId'], 'amount' =>(float) $request['final_amount'], 'transferId' => $request['order_id'],
                        'user_name' => $request['user_name'], 'full_name' => $request['full_name'], 'bankAccount' => $request['ac_number'], 'ifsc' => $request['ifsc_code']];
                    $status = requestTransfer($body, $request['order_id']);
                    if($status){
                       $balance = ($balance-$request['winning_amount']);
                    }
                }
            }
        }
        return true;
    }
}


if(!function_exists('checkBeneficiary')){
    /** --- first check the bank detail on cashfree if found then check beneId is same as user_unique_id if not then delete first from cashfree then add into new process -- **/
    function checkBeneficiary($data)
    {
        $ci = ci();
        $ci->load->database();
        $ci->load->model('CashfreeBeneficiary_model');
        $body = false;
        $beneficiaryRecord = $ci->CashfreeBeneficiary_model->getRecords($data['user_unique_id']);
        if(empty($beneficiaryRecord))
        {
            $body = addBeneficiary($data, 0);
        }
        else
        {
            $ifscMatched = 0; 
            foreach($beneficiaryRecord as $beneficiary){
                if((strtolower($beneficiary['ifsc']) == strtolower($data['ifsc_code'])) && ($beneficiary['bankAccount'] == $data['ac_number'])){
                    $ifscMatched = 1;
                    $body = $beneficiary;
                    break;
                }
            }
            if(empty($ifscMatched)){
                $body = addBeneficiary($data, count($beneficiaryRecord));
            }
        }
        return $body;
    }
}


if(!function_exists('addBeneficiary')){
    function addBeneficiary($data, $counter, $isNested = false)
    {
//        echo PHP_EOL. "INSIDE addBeneficiary : $isNested ";
        $ci = ci();
        $ci->load->database();
        $ci->load->model('CashfreeBeneficiary_model');
        
        if(empty($data['ifsc_code']) || empty($data['ac_number'])){
            return false;
        }
        $beneId = ($counter > 0 )? $data['user_unique_id']."_".$counter: $data['user_unique_id'];       // we are using counter for make unique beneId for each bank detail on cashfree 
        $body = ['beneId' => $beneId, 'name' => $data['full_name'], 'email' => $data['email'], 'phone' => $data['phone_no'], 
                'bankAccount' => $data['ac_number'], 'ifsc' => $data['ifsc_code'], 'address1' => !empty($data['user_address'])?$data['user_address']:'Null Address', 'city' => "Gurgaon", 'state' => "Haryana"]; 
        // --------- add beneficiary record on cashfree platform
        $response = cashfree_addBeneficiary($body);
        if(!empty($response) && $response['subCode'] == 200)
        {
            unset($body['state']);
            $body['user_id'] = $data['user_id'];
            $body['user_unique_id'] = $data['user_unique_id'];
            
            // --------- add cashfree beneficiary detail on Local DB
            $ci->CashfreeBeneficiary_model->insert($body);
            return $body;
        }
        else if(!empty($response) && $response['subCode'] == 409)
        {
            // 409 -> Entered bank Account is already registered
            if($isNested){ // if addBeneficiary() is call inside same function so we will terminate this function means its not a valid Bank detail.
                return false;
            }
            $beneDetail = cashfree_getBeneId($data['ac_number'], $data['ifsc_code']);
            if(!empty($beneDetail) && $beneDetail['subCode'] == 200 && !empty($beneDetail['data'])){
                
                $removeBeneResponse = cashfree_removeBeneficiary($beneDetail['data']['beneId']);
                if(!empty($removeBeneResponse) && $removeBeneResponse['subCode'] == 200){
                    return addBeneficiary($data, $counter, true);   // again try to add this bank beneficiary for new user_unique_id
                }
            }
        }
        return false;
    }
}

if(!function_exists('requestTransfer')){
    function requestTransfer($body, $oderId, $latestOrderId = NULL)
    {
//        echo PHP_EOL."-- Inside requestTransfer".PHP_EOL;
        if(empty($body['beneId']) || empty($body['amount']) || empty($oderId)){
            return false;
        }
        $ci = ci();
        $ci->load->database();
        $ci->load->model('Withdrawal_model');
        
        // ------------- check this transcation is already proccessed or not from withdrawal table
        $isAlreadyProcessedOrder = $ci->Withdrawal_model->getOrderDetail($oderId);
        
        // -------- in case of reprocess we need to send new $latestOrderId  for again try with new id for same withdrawal
        if(!empty($isAlreadyProcessedOrder) && is_array($isAlreadyProcessedOrder)){
            if(!empty($latestOrderId) &&  $latestOrderId == $isAlreadyProcessedOrder['latest_transferId'])
            {
                return false;               // order already processed. we need to check the status of processed transaction.
            }else if(empty($latestOrderId)){
                return false;
            }
        }
        
        $requestOrderId = (!empty($isAlreadyProcessedOrder) && !empty($latestOrderId) &&  $latestOrderId != $isAlreadyProcessedOrder['latest_transferId'])? $latestOrderId: $oderId;
        
        $requestBody = ['beneId' => $body['beneId'], 'amount' => (float) $body['amount'], 'transferId' => $requestOrderId];
        $response = cashfree_requestTransfer($requestBody);
        if(!empty($response)){
            
            $body['status'] = strtolower($response['status']);
            $body['status_code'] = $response['subCode'];
            $body['message'] = $response['message'];
            $body['latest_transferId'] = $requestOrderId;
            if(!empty($response['data']) && !empty($response['data']['referenceId'])){
                $body['referenceId'] = $response['data']['referenceId'];
            }
            
            if(!empty($isAlreadyProcessedOrder)){
                $body['process_attempt'] = $isAlreadyProcessedOrder['process_attempt']+1;
                $ci->Withdrawal_model->updateDocument($body, ['id' => $isAlreadyProcessedOrder['id']]);
            }else{
                $body['created_at'] = date_time_in_ist(date('Y-m-d H:i:s'));
                $ci->Withdrawal_model->insert($body);
            }
            if($response['subCode'] == 200){
                // update on fantasy akhada api for regarding successfully transaction processed
                faChangeWithdrawalStatus($oderId, 1);
                return true;
            }
        }
        return (in_array(strtolower($response['status']), ['success', 'pending']))? true: false;
    }
}



if(!function_exists('faChangeWithdrawalStatus')){
    function faChangeWithdrawalStatus($orderId, $status, $reason = ""){
        
        $ci = ci();
        $url = "https://www.fantasyakhada.com/adminapi/index.php/finance/change_withdrawal_status";
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$ci->config->item('fa_session_id')];
        $result = curl_request($url, $header, 'POST', ['order_id' => $orderId, 'status' => $status, "index" => 0, "reason" => $reason, 'action' => '', 'selectall' => '', 'withdraw_transaction_id' => [], 'description'=>'']);
        return $result;
    }
    
}