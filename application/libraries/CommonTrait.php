<?php

trait CommonTrait
{
       
    public function findUserContest(){
        $this->load->model('ContestTrackUserList_model');
        $this->load->model('TrackUserContest_model');
        
        // ------------ get all active user from contest_track_user_list table
        $users = $this->ContestTrackUserList_model->getActiveUsers();
        $allUsersContest = [];
        if(!empty($users)){
            foreach ($users as $user){
                // ---- get all contest of user on same day from Fantasy Akhada API.
                $upComingContest = $this->userContestDetail($user['user_id'], $user['name']);
                if(!empty($upComingContest)){
                    $allUsersContest[$user['user_id']] = $upComingContest;
                }
            }
        }
        
        $emailSendFlag = 0;
        if(!empty($allUsersContest)){
            $bulkInsert = [];
            $emailHtml = '<html><body><table style="border:1px solid black;border-collapse:collapse;" ><tbody style="border:1px solid black;">';
            $emailHtml .= '<tr style="border:1px solid black;">    <th style="border:1px solid black;">User Name</th>    <th style="border:1px solid black;">Group Name</th>    <th style="border:1px solid black;">Contest Name</th>    <th style="border:1px solid black;">Match</th>    <th style="border:1px solid black;">Size</th>  <th style="border:1px solid black;">Time</th>   </tr>';
            
            foreach ($allUsersContest as $userId => $contestDetails){
                // ------- get exists record, so that we can remove contest from sent email means already email is sent regarding contest to team 
                $existsRecord = $this->TrackUserContest_model->getUserContestRecord($userId, array_values(array_column($contestDetails, 'contest_id')));
                
                // ------------- prepare email template for new contest 
                foreach ($contestDetails as $singleContest){
                    if(!empty($existsRecord) && in_array($singleContest['contest_id'], array_column($existsRecord, 'contest_id'))){
                        continue;
                    }
                    $emailSendFlag = 1;
                    $emailHtml .= '<tr style="border:1px solid black;">    <td style="border:1px solid black;">'.$singleContest['user_name'].'</td>    <td style="border:1px solid black;">'.$singleContest['group_name'].'</td>    <td style="border:1px solid black;">'.$singleContest['contest_name'].'</td>    <td style="border:1px solid black;">'.$singleContest['title'].'</td>    <td style="border:1px solid black;">'.$singleContest['total_user_joined'].'/'.$singleContest['size'].'</td>  <td style="border:1px solid black;">'. gmdate('Y-m-d H:i A', strtotime($singleContest['season_schedule_date'] . '+ 330 minutes' )).'</td>    </tr>';
                    $bulkInsert[] = ['user_id' => $userId, 'user_name' => $singleContest['user_name'], 'group_name' => $singleContest['group_name'], 'title' => $singleContest['title'], 'contest_id' => $singleContest['contest_id'], 'contest_name' => $singleContest['contest_name']]; 
                }
            }
            $emailHtml .= '</tbody></table></body></html>';
        }
        
        if($emailSendFlag){
            // ----- bulk insert user new contest because it will never send again to the team
            $this->TrackUserContest_model->bulInsert($bulkInsert);
            
            // ---------- send email to team
            send_email('sahil.ahuja@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('jackson.bodra@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('aman.thakur@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('akshay.purohit@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('paawan.mehta@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('varun.paruchuri@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('amit.purohit@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            sleep(2);
            send_email('fabp@supersixsports.com', 'Track Users Contest - '.gmdate('Y-m-d', strtotime('+ 330 minutes' )), $emailHtml);
            $bulkInsert = [];$emailHtml = '';
        }
    }
    
    
    private function userContestDetail($userId, $name){
        
        $url = 'https://www.fantasyakhada.com/adminapi/index.php/user/user_game_history/';
        $header = [ 'Content-Type:application/json', 'sessionkey:'.$this->config->item('fa_session_id')];
        $body = ['items_perpage' => 100, 'total_items' => 50, 'current_page' => 1, 'sort_order' => 'DESC', 'sort_field' => 'season_scheduled_date', 'user_id' => $userId,
            'from_date' => gmdate('Y-m-d'), 'to_date' => gmdate('Y-m-d', strtotime( '+ 24 hours' ))  ];
        
        // --------- curl request to Fantasy Akhada
        $requestData = curl_request($url, $header, 'POST', $body);
        
        $upcomingContest = [];
        if(!empty($requestData) && !empty($requestData['data']) && !empty($requestData['data']['result'])){
            foreach ($requestData['data']['result'] as $contest){
                if( (strtotime(gmdate('Y-m-d H:i:s', strtotime('+5 minutes'))) < strtotime($contest['season_schedule_date']))  && ($contest['total_user_joined'] < $contest['size']) ){
                    
                    // ------ avoid list of contest from Email
                    if(in_array( strtolower($contest['group_name']), ['practice contest', 'mega contest', 'private contest'] ) ){
                        continue;
                    }
                    
                    $upcomingContest[] = ['user_name' => $name, 'contest_id' => $contest['contest_id'] , 'group_name' => $contest['group_name'] , 'contest_name' => $contest['contest_name'] , 'title' => $contest['title'], 'total_user_joined' => $contest['total_user_joined'], 'size' => $contest['size'], 'season_schedule_date' => $contest['season_schedule_date']];
                }
            }
        }
        return $upcomingContest;
    }
}
