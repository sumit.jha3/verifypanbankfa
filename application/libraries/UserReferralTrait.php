<?php

trait UserReferralTrait
{
    protected $size = 25;
    private function UserReferralDependency(){
        $this->load->model('ReferralAmountDistribution_model');
        $this->load->helper('user_helper');
    }
    
    
    public function usersReferral(){
        $this->UserReferralDependency();
        
//        $refs[] = ['referredUserId' => "459063",'start_date' => "2022-03-01", 'end_date' => "2022-03-01", 'referredUserUniqueId' => "f7506f3e24", 'given_to' => 'owner', 'value' => 50, 'type' => 'REAL_CASH'];
//        $refs[] = ['referredUserId' => "8",'start_date' => "2022-03-01", 'end_date' => "2022-03-01", 'referredUserUniqueId' => "0cc145ebef", 'given_to' => 'user', 'value' => 50, 'type' => 'REAL_CASH'];  // Pratik Gosar [founder]
//        $refs[] = ['referredUserId' => 8,'start_date' => "2022-03-01", 'end_date' => "2022-03-01", 'referredUserUniqueId' => "0cc145ebef", 'given_to' => 'user', 'value' => 4500, 'type' => 'BONUS_CASH'];  // Pratik Gosar [founder]
        $refs[] = ['referredUserId' => "1552555", 'referredUserUniqueId' => "f048316371", 'start_date' => "2022-02-01", 'end_date' => "2022-02-08", 'given_to' => 'user', 'value' => 50, 'type' => 'REAL_CASH'];  // Aakaash Chopra [Hindi Cricket commentator ]
        foreach($refs as $ref){
            $this->userReferralAmountDistribution(1, $ref);
        }
        
    }

    public function userReferralAmountDistribution($page, $ref){
        echo PHP_EOL. "userReferralAmountDistribution: $page".PHP_EOL;          print_r($ref);
                
        if(isset($ref['start_date']) && ( strtotime($ref['start_date'])  <= strtotime(gmdate('Y-m-d'))) &&  (strtotime(gmdate('Y-m-d')) <= strtotime($ref['end_date']) )   ){
            $userRecords = getReferralUsers($ref['referredUserId'], $this->size, $page, $ref['start_date'], $ref['end_date']);
    //        $referredUserId = "355050";    $referredUserUniqueId = "87beb6bcae";   // bharat detail for testing

            if(!empty($userRecords) && !empty($userRecords['data']) && !empty($userRecords['data']['referral_list']) && !empty($userRecords['data']['referral_list']['result']) ){
                $referralUsers = $userRecords['data']['referral_list']['result'];
                $totalCount = $userRecords['data']['referral_list']['total'];


                $this->processUserReferralData($ref['referredUserId'], $ref['referredUserUniqueId'], $referralUsers, $ref['value'], $ref['type'], $ref['given_to']);

                // ----------- nested loop for process all pages  -----------------//
                $totalPage = ceil($totalCount/$this->size);
                echo PHP_EOL. "Total Records : $totalCount && Current Page is : $page && Total Page Count $totalPage  & referredUserId: ".$ref['referredUserId'].PHP_EOL;
                if($page+1 <= $totalPage){
                    $this->userReferralAmountDistribution($page+1, $ref);
                }
            }   
        }
    }
    
    protected function processUserReferralData($referredUserId, $referredUserUniqueId, $referralUsers, $amount, $type, $givenTo){
        if(empty($referralUsers) || empty($referredUserId)){
            return false;
        }        
        
        foreach($referralUsers as $user){                    
            $existsRecord = $this->ReferralAmountDistribution_model->getUserReferralDetail($referredUserId, $user['user_unique_id']);
            if(empty($existsRecord))
            {
                // $referredUserId = "355050";    $referredUserUniqueId = "87beb6bcae";   // bharat detail for testing
                // $user['user_unique_id'] =  "87beb6bcae"; $user['user_id'] = "355050";  // bharat detail for testing
                
                if(!empty($givenTo) && ($givenTo == 'owner')){
                    $userDetail = getUserDetail($user['user_unique_id']);

                    if(!empty($userDetail) && !empty($userDetail['data']) && $userDetail['data']['is_bank_verified'] == 1 && $userDetail['data']['pan_verified'] == 1){
                        echo PHP_EOL." Referred User Unique Id: $referredUserUniqueId and amount credit is : $amount ".PHP_EOL;

                        addUserBalance(['transaction_amount_type' => $type, 'user_unique_id' => $referredUserUniqueId, 'user_balance_reason' => 'Referral benefits - amount '.$amount , 'amount' => $amount, 'transaction_type' => 'CREDIT']);

                        $this->ReferralAmountDistribution_model->insert([ 'referred_user_id' => $referredUserId, 'user_id' => $user['user_id'],
                            'user_unique_id' => $user['user_unique_id'], 'amount_distribution' => $amount, 'pan_verified' => 1, 'bank_verified' => 1, 'type' => $type ]);
                    }
                }else if(!empty($givenTo) && ($givenTo == 'user')){
                    if($type == 'BONUS_CASH'){
                        addUserBalance(['transaction_amount_type' => $type, 'user_unique_id' => $user['user_unique_id'], 'user_balance_reason' => 'Bonus Cash Added -  '.$amount , 'amount' => $amount, 'transaction_type' => 'CREDIT']);

                        $this->ReferralAmountDistribution_model->insert([ 'referred_user_id' => $referredUserId, 'user_id' => $user['user_id'], 'user_unique_id' => $user['user_unique_id'], 'amount_distribution' => $amount, 'type' => $type ]);
                    }else if($type == 'REAL_CASH'){
                        addUserBalance(['transaction_amount_type' => $type, 'user_unique_id' => $user['user_unique_id'], 'user_balance_reason' => 'Real Cash Added -  '.$amount , 'amount' => $amount, 'transaction_type' => 'CREDIT']);
                        $this->ReferralAmountDistribution_model->insert([ 'referred_user_id' => $referredUserId, 'user_id' => $user['user_id'], 'user_unique_id' => $user['user_unique_id'], 'amount_distribution' => $amount, 'type' => $type ]);
                    }
                }
            }
        }
    }
}