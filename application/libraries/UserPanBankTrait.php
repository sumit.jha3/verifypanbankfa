<?php

trait UserPanBankTrait
{
    private function loadDependency(){
        $this->load->helper('user_pan_bank_helper');
        $this->load->model('FaUserPanBank_model');
    }


    public function reprocessSubmitBankData(array $ids, array $otherData = []){
        if(empty($ids))
            return false;
        
        if(!empty($otherData) && !empty($otherData['rejected']) && empty($otherData['message'])){
            return false;
        }

        $this->loadDependency();
        $bankRecord = $this->FaUserPanBank_model->getAllRecord(['ids' => $ids, 'size' => count($ids)]);

        $bank_verified = "1";
        $bank_rejected_reason = '';
        $message = json_encode(['bank_process' => ['message' => 'Manually Approved.']]);
        if(!empty($otherData) && !empty($otherData['rejected']) && !empty($otherData['message'])){
            $bank_verified = "2";
            $bank_rejected_reason = "Manually Rejected : ".$otherData['message'];
            $message = Null;
        }
        
        
        foreach($bankRecord as $data){

            verifyUserBankApiFA(['user_unique_id' => $data['user_unique_id'], 'bank_verified' => $bank_verified, 'bank_rejected_reason' => $bank_rejected_reason]);
            
            $userDetail = [];
            $userDetail['bank_verified'] = ($bank_verified == 1)? 1:0;
            $userDetail['bank_processed'] = 1;
            $userDetail['bank_rejected_reason'] = $bank_rejected_reason;
            $userDetail['extra_data'] = $message;
            
            $this->FaUserPanBank_model->updateDocument($userDetail, ['id' => $data['id']]);
        }
        return true;
    }
    
    
    public function deleteUserPanBankRecord(array $ids){
        if(!empty($ids)){
            foreach($ids as $id){
                $userDetail = [];
                $userDetail['deleted'] = 1;
                $this->FaUserPanBank_model->updateDocument($userDetail, ['id' => $id]);
            }
        }
        return true;
    }
    
    
    public function processPanBankDataFromFaTrait(){
        $this->loadDependency();

        $response = get_pending_user_pan_record();
        if(!empty($response) && !empty($response['data']) && !empty($response['data']['result'])){
            $allRecord = $response['data']['result'];
            $bulkInsert = [];
            foreach($allRecord as $result)
            {
                if(empty($result['user_unique_id']) || empty($result['dob'])  || empty($result['user_id']))
                    continue;
                                
                $userInfo = $this->FaUserPanBank_model->getUserDetail($result['user_unique_id']);
                
                if(empty($userInfo)){

                    $userDetail = $this->userMapping($result);
                    
                    // 1. first verify PAN then BANK. If pan is not valid then ignore bank process.
                    if(!empty($userDetail['pan_no']) && strlen($userDetail['pan_no']) == 10 ){
                        if(empty($userDetail['pan_verified'])){
                            $validPan = $this->processPan($userDetail, $result);
                            if($validPan){
                                updatePanVerifiedApiFA($userDetail['user_unique_id']);
                                $userDetail['pan_verified'] = 1;
                                
                                $validBank = $this->bankProcess($userDetail, $result);
                                if($validBank){
                                    verifyUserBankApiFA(['user_unique_id' => $result['user_unique_id'], 'bank_verified' => "1", 'bank_rejected_reason' => ""]);
                                    $userDetail['bank_verified'] = 1;
                                }
                                $userDetail['bank_processed'] = $userDetail['is_processed'] = 1;
                            }
                            $userDetail['pan_processed'] = 1;
                        }
                        else if(empty($userDetail['bank_verified']))
                        {
                            $validBank = $this->bankProcess($userDetail, $result);
                            if($validBank){
                                verifyUserBankApiFA(['user_unique_id' => $result['user_unique_id'], 'bank_verified' => "1", 'bank_rejected_reason' => ""]);
                                $userDetail['bank_verified'] = 1;
                            }
                            $userDetail['is_processed'] = $userDetail['bank_processed'] = 1;
                        }
                    }
                    unset($userDetail['bank_document']);
                    $this->FaUserPanBank_model->insert($userDetail);
                }
                else{
                    $this->againProcessPanBank($result, $userInfo);
                }
                
               
            }
        }
    }
    
    public function userMapping($result)
    {
        $data['user_id'] = $result['user_id'];
        $data['user_unique_id'] = $result['user_unique_id'];
        $data['is_processed'] = 0;
        $data['first_name'] = !empty($result['first_name'])?$result['first_name']:"";
        $data['last_name'] = !empty($result['last_name'])?$result['last_name']:"";
        $data['phone_verfied'] = (int) $result['phone_verfied'];
        $data['email_verified'] = (int) $result['email_verified'];
        $data['bank_verified'] = (int) $result['is_bank_verified'];
        $data['dob'] = !empty($result['dob'])?$result['dob']:"";
        $data['pan_no'] = (string)$result['pan_no'];
        $data['pan_verified'] = (int)$result['pan_verified'];
        $data['added_date'] = date('Y-m-d H:i:s');
        $data['bank_document'] = (string)$result['bank_document'];
        $data['bank_processed'] = 0;
        $data['pan_processed'] = 0;
        
        return $data;
    }
    
    public function processPan(&$userDetail, $result){
        if(empty($userDetail['dob'])){
            $userDetail['pan_rejected_reason'] = "dob is missing";
            return false;
        }
        $name = $userDetail['first_name'];
        $changedName = false;
        if(empty($name)){
            $name = $this->getNameFromPANHyperVerge($userDetail['pan_no']);
            if(empty($name)){
                $userDetail['pan_rejected_reason'] = "Name: false from HyperVerge";
                return false;
            }
            $changedName = true;
        }
        
        // ----------- hyper verge api to verify PAN --------------------------------
        $verifyPANResult = $this->hyperverge_verifyPAN($userDetail, $name);
        if(!empty($verifyPANResult) && isset($verifyPANResult['statusCode']) && $verifyPANResult['statusCode'] == 200)
        {
            if($verifyPANResult['result']['status'] == 'Active'){
                if($verifyPANResult['result']['dobMatch'] != true){
                    $userDetail['pan_rejected_reason'] = "dobMatch: false from HyperVerge";
                    return false; // DOB not match
                }else{
                    $ageObj = ageCalculate($userDetail['dob']);
                    if(!empty($ageObj) && $ageObj->y < 18){
                        $userDetail['pan_rejected_reason'] = "Age is below 18 year : ".$userDetail['dob'];
                        return false;
                    }
                }
                
                if($verifyPANResult['result']['nameMatch'] != true){
                    if($changedName == true){
                        $userDetail['pan_rejected_reason'] = "User Name is not matched from HyperVerge. Name is : ". $name;
                        return false;   // pan name is not correct from verifyPAN api and i already checked name from Hyper verge getNameFromPAN API.
                    }else{
                        $name = $this->getNameFromPANHyperVerge($userDetail['pan_no']);
                        if(empty($name)){
                            $userDetail['pan_rejected_reason'] = "Name: false from HyperVerge";
                            return false;
                        }
                        $changedName = true;
                    }
                }
            }
            else{
                $userDetail['pan_rejected_reason'] = "PAN not active from HyperVerge";
                return false; //PAN is not valid
            }
        }
        else{
//            $userDetail['pan_rejected_reason'] = "HyperVerge Response ". (empty($verifyPANResult)? ' result is empty.': " code is ".$verifyPANResult['statusCode']. " PAN Number is not present in DB.");
            $userDetail['pan_rejected_reason'] = "HyperVerge Response ". json_encode($verifyPANResult);
            return false;
        }
        
        if($changedName){
            // update name on Fantasy Akhada api
            updatePanInfoApiFA(['pan_no' => $userDetail['pan_no'], 'first_name' => $name, 'last_name' => $result['last_name'], 'user_unique_id' =>  $result['user_unique_id'], 'dob' => $result['dob'], 'pan_image' => $result['pan_image']]);
            $userDetail['first_name'] = $name;
        }
        return true;
    }
    
    
    public function getNameFromPANHyperVerge($pan){
        if(empty($pan))
            return "";
        
        $hyperVergeConfig = $this->config->item('hyper_verge');
        $body = ['pan' => $pan];
        $url = $hyperVergeConfig['base_url'].$hyperVergeConfig['getNameFromPAN'];
        $result = curl_request($url, $hyperVergeConfig['header'], 'POST', $body);
        // response result sample : {"status":"success","statusCode":"200","result":{"name":"SUMIT KUMAR JHA"}}
        
        if(!empty($result) && isset($result['statusCode']) && $result['statusCode'] == 200){
            return $result['result']['name'];
        }
        return "";
    }
    
    public function hyperverge_verifyPAN($userDetail, $name){
        $hyperVergeConfig = $this->config->item('hyper_verge');
        $body = ['pan' => $userDetail['pan_no'], 'name' => $name, 'dob' => date('d/m/Y', strtotime($userDetail['dob']))];
        $url = $hyperVergeConfig['base_url'].$hyperVergeConfig['verifyPAN'];
        $verifyPANResult = curl_request($url, $hyperVergeConfig['header'], 'POST', $body);
        // response result sample : {"status":"success","statusCode":"200","result":{"status":"Active","duplicate":null,"nameMatch":true,"dobMatch":true}}
        return $verifyPANResult;
    }
    
    
    public function bankProcess(&$userDetail, $result){
        if($userDetail['pan_verified'] != 1){
            $userDetail['bank_rejected_reason'] = "Pan is not verified";
            return false;
        }
        // --------- get user bank detail -------------------
        $bankFaApi = get_user_bank_detail($result['user_id']);
        // response Result : {"service_name":"get_user_bank_data","message":"","global_error":"","error":[],"data":{"first_name":"bharat","last_name":"","bank_name":"syndicate","ac_number":"33455299763","ifsc_code":"SBIF0987613","micr_code":null,"upi_id":""},"response_code":200}
       
        if(!empty($bankFaApi) && ($bankFaApi['response_code'] == 200) && !empty($bankFaApi['data'])){
            $bankNumber = $bankFaApi['data']['ac_number'];
            $ifscCode = $bankFaApi['data']['ifsc_code'];
            $ifscCodeValidation = $this->ifscCodeValidation($ifscCode);
            if(empty($ifscCodeValidation) ||  $ifscCodeValidation['status'] != true){
                $userDetail['bank_rejected_reason'] = !empty($ifscCodeValidation['message'])? $ifscCodeValidation['message']: "Invalid Ifsc Code";
                $userDetail['bank_ifsc_code'] = $ifscCode;
                return false;
            }
            $ifscCode = $ifscCodeValidation['ifscCode'];                    // if ifsc code is changed then replace here with new/corrected ifsc code.

            $userDetail['bank_number'] = $bankNumber;
            $userDetail['bank_ifsc_code'] = $ifscCode;
            $userDetail['bank_name'] = $bankFaApi['data']['bank_name'];
            $userDetail['bank_account_name'] = $bankFaApi['data']['first_name'];
            
            // ------------ call Hyper Verge Api to check it is valid Account or not. -------------------
            $bankRespose = $this->hyperverge_checkBankAccount($ifscCode, $bankNumber);
            if(!empty($bankRespose) && isset($bankRespose['statusCode']) && $bankRespose['statusCode'] == 200 && !empty($bankRespose['result']['bankTxnStatus']) )
            {
                $userDetail['bank_account_name'] = $bankRespose['result']['accountName'];
                if($userDetail['first_name'] != $bankRespose['result']['accountName'])
                {
                    //-------- check final first_name and bank name is same or not
                    $isNameMatched = $this->hyperverge_matchFields($userDetail['first_name'], $bankRespose['result']['accountName']);
                    if( !$isNameMatched ){
                        $userDetail['extra_data'] = json_encode([ 'bank_process' => ['pan_name' => $userDetail['first_name'], 'user_name_in_bank' => $bankRespose['result']['accountName']] ]);
                        $userDetail['bank_rejected_reason'] = "first_name and bank account name is not matched";
                        return false;
                    }
                }
                // ----------- update user bank detail on FA api
                $updateDetail = update_user_bank_detail(['first_name' => $bankRespose['result']['accountName'], 'last_name' => $result['last_name'], 'bank_name' => $bankFaApi['data']['bank_name'], 'ifsc_code' => $ifscCode, 'ac_number' => $bankNumber, 'user_unique_id' => $userDetail['user_unique_id'], 'bank_document' => $result['bank_document']]);

                return true;
            }
            else if(!empty($bankRespose) && isset($bankRespose['statusCode'])){
                // check here for OCR api for getting bank account number from image to check again.
                $userDetail['bank_verified'] = 0;
                $userDetail['bank_rejected_reason'] = "Hyper Verge Bank Response: ".((!empty($bankRespose['result'])&& !empty($bankRespose['result']['bankResponse']))?$bankRespose['result']['bankResponse']:$bankRespose['statusCode']);
            }
        }
        return false;
    }
    
    public function ifscCodeValidation($ifscCode)
    {
        $len = strlen($ifscCode);
        if($len != 11)
            return ["status" => false];
        
        $isChanged = false;
        // ---------------- check starting 4 value must me char and 5th position must be number. if its 'o' then replace with '0'  ----------------
        for($i = 0; $i < $len; $i++){
            if($i < 4 && is_numeric($ifscCode[$i])){
                return ["status" => false];
            }
            else if($i == 4 && !is_numeric($ifscCode[$i]) && $ifscCode[$i] == 'o'){
                $ifscCode[$i] = 0;
                $isChanged = true;
            }
            elseif($i > 4){
                break;
            }
        }
        
        // ----- check Ifsc code is in our database or not. first check from new ifsc code table, if not found then check old new ifsc code mapping table -----
        $rbiIfscCode = $this->db->where('ifsc_code', $ifscCode)->get('rbi_ifsc_code');
        if($rbiIfscCode->num_rows() == 0){
            $oldNewIfscCode = $this->db->where('old_ifsc_code', $ifscCode)->get('old_to_new_ifsc_code');
            if($oldNewIfscCode->num_rows()){
                $mapping = $oldNewIfscCode->result_array();
                $ifscCode =  $mapping[0]['new_ifsc_code'];
                $isChanged = true;
            }
            else{
                return ["status" => false, 'message' => 'Ifsc Code Not Found In IFSC Table'];           // IFSC code is invalid
            }
        }
        return ["status" => true, 'isIfscCodeChange' => $isChanged, 'ifscCode' => $ifscCode];
    }
    
    protected function hyperverge_checkBankAccount($ifscCode, $bankNumber){
        $hyperVergeConfig = $this->config->item('hyper_verge');
        $url = $hyperVergeConfig['base_url'].$hyperVergeConfig['checkBankAccount'];
        $bankRespose = curl_request($url, $hyperVergeConfig['header'], 'POST', ['ifsc' => $ifscCode, 'accountNumber' => $bankNumber]);
        // bank respose : {"status":"success","statusCode":"200","result":{"bankResponse":"Transaction Successful","ifsc":"CNRB0012813","accountNumber":"88432010006285","accountName":"Bharat","bankTxnStatus":true}}
        return $bankRespose;
    }
    
    protected function hyperverge_matchFields($name1, $name2){
        // -------- check name is corrected or not https://ind-verify.hyperverge.co/api/matchFields  --------------
        $hyperVergeConfig = $this->config->item('hyper_verge');
        $url = $hyperVergeConfig['base_url'].$hyperVergeConfig['matchFields'];
        $matchFieldRespose = curl_request($url, $hyperVergeConfig['header'], 'POST', ['name' => ['value1' => $name1, 'value2' => $name2, "preferences" => ['removeSuffix' => 'yes', 'phonetic' => 'yes', "editDistance" => 2] ] ]);
        // bank respose : {"status":"success","statusCode":"200","result":{"name":true,"all":true}}
        return (!empty($matchFieldRespose) && $matchFieldRespose['statusCode'] == 200 && $matchFieldRespose['result']['name'] == true)? true : false;
    }
    
    
    protected function againProcessPanBank($faPendingData, $userInfo){
        // ------------ single record processing ---------------------------
        if($faPendingData['user_unique_id'] != $userInfo['user_unique_id']){
            return false;
        }
        $rejectedPan = $rejectedBank = 0;
        if($faPendingData['pan_verified'] == 1){
            if($userInfo['pan_verified'] == 0){
                $userInfo['pan_processed'] = $userInfo['pan_verified'] = 1;
                $userInfo['pan_rejected_reason'] = '';
                $userInfo['pan_no'] = $faPendingData['pan_no'];
                $userInfo['dob'] = date('Y-m-d', strtotime($faPendingData['dob']));
                $userInfo['first_name'] = $faPendingData['first_name'];
            }
        }else{
            if($userInfo['pan_verified'] == 1){
                // ---------- if PAn is verified on Verify System so need to update on FA system.
                updatePanVerifiedApiFA($userInfo['user_unique_id']);
            }else {
                if( (strtolower($faPendingData['pan_no']) != strtolower($userInfo['pan_no']))  || (strtotime($faPendingData['dob']) != strtotime($userInfo['dob']))   || (strtolower($faPendingData['first_name']) != strtolower($userInfo['first_name'])) ){
                    // -------------- process Pan Here --------------------
                    $userInfo['pan_no'] = $faPendingData['pan_no'];             // replace old value with new exists value
                    $userInfo['dob'] = $faPendingData['dob'];                   // replace old value with new exists value
                    $userInfo['first_name'] = $faPendingData['first_name'];     // replace old value with new exists value
                    
                    $validPan = $this->processPan($userInfo, $faPendingData);
                    if($validPan){
                        updatePanVerifiedApiFA($userInfo['user_unique_id']);
                        $userInfo['pan_verified'] = 1;
                    }
                    $userInfo['pan_processed'] = 1;
                }else{
                     $rejectedPan = 1;                                           //--------------- reject PAN
                }
            }
        }
        if($rejectedPan){
            // updatePanVerifiedApiFA($userInfo['user_unique_id'], 2, "Invalid PAN Details.");
        }
        
        
        // ---------------- bank process code here ------------
        if($userInfo['pan_verified'] == 1 && $userInfo['bank_verified'] != 1){
            $bankFaApi = get_user_bank_detail($faPendingData['user_id']);
            // response Result : {"service_name":"get_user_bank_data","message":"","global_error":"","error":[],"data":{"first_name":"bharat","last_name":"","bank_name":"syndicate","ac_number":"33455299763","ifsc_code":"SBIF0987613","micr_code":null,"upi_id":""},"response_code":200}
            
            if($faPendingData['is_bank_verified'] == 1){
                if(!empty($bankFaApi) && ($bankFaApi['response_code'] == 200) && !empty($bankFaApi['data'])){
                    $userInfo['bank_number'] = $bankFaApi['data']['ac_number'];
                    $userInfo['bank_ifsc_code'] = $bankFaApi['data']['ifsc_code'];
                    $userInfo['bank_name'] = $bankFaApi['data']['bank_name'];
                    $userInfo['bank_account_name'] = $bankFaApi['data']['first_name'];
                    $userInfo['bank_rejected_reason'] = '';
                    $userInfo['bank_verified'] = 1;
                }
            }
            else
            {
                if(!empty($bankFaApi) && ($bankFaApi['response_code'] == 200) && !empty($bankFaApi['data']) && !empty($bankFaApi['data']['ifsc_code']) && !empty($bankFaApi['data']['ac_number']) && !empty($bankFaApi['data']['first_name'])){
                    if($userInfo['bank_rejected_reason'] != 'first_name and bank account name is not matched'){
                        if( ($bankFaApi['data']['ac_number'] != $userInfo['bank_number']) || ($bankFaApi['data']['ifsc_code'] != $userInfo['bank_ifsc_code']) || ($bankFaApi['data']['first_name'] != $userInfo['first_name'])){
                            
                            $userInfo['bank_number'] = $bankFaApi['data']['ac_number'];         // replace old value with new exists value
                            $userInfo['bank_ifsc_code'] = $bankFaApi['data']['ifsc_code'];      // replace old value with new exists value
                            $userInfo['first_name'] = $bankFaApi['data']['first_name'];         // replace old value with new exists value
                            
                            // -------------- process Bank Here --------------------
                            $validBank = $this->bankProcess($userInfo, $faPendingData);
                            if($validBank){
                                verifyUserBankApiFA(['user_unique_id' => $faPendingData['user_unique_id'], 'bank_verified' => "1", 'bank_rejected_reason' => ""]);
                                $userInfo['bank_verified'] = 1;
                            }
                        }else{
                            $rejectedBank = 1;
                        }
                        $userInfo['bank_processed'] = $userInfo['is_processed'] = 1;
                    }
                }
            }
            
            
        }
        if($rejectedBank){
            $userInfo['bank_verified'] = 0;
            // verifyUserBankApiFA(['user_unique_id' => $faPendingData['user_unique_id'], 'bank_verified' => "2", 'bank_rejected_reason' => "Invalid BANK Details."]);
        }
        
        $id = $userInfo['id'];
        unset($userInfo['id']);
        $this->FaUserPanBank_model->updateDocument($userInfo, ['id' => $id]);
    }
}