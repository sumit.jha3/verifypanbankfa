<?php

trait EntitySportsTrait
{
    
    private function loadEntitySportsTraitDependency(){
        $this->load->model('CricketMatches_model');
        $this->load->model('CricketScorecard_model');
    }
    
    
    public function completeMatchSquad(){
        $this->loadEntitySportsTraitDependency();
        $matches = $this->CricketMatches_model->getIncomleteSquadMatches();
        if(!empty($matches) && is_array($matches)){
            foreach($matches as $match){
                $this->updateMatchSquad($match['match_id']);
            }
        }
    }
    
    
    protected function updateMatchSquad($matchId){
        if(empty($matchId) ){
           return false;
        }
        $this->load->model('CricketMatches_model');
        
        $env = is_production_env()?'production':'development';
        $entitySportConfig = $this->config->item('entitysport');
        $token = $entitySportConfig['token'][$env];
        
       
        
        $url = $entitySportConfig['host']. "matches/$matchId/squads?token=$token";      // https://rest.entitysport.com/v2/matches/{mid}/squads?token=
        $apiResponse = curl_request($url, ['Content-Type:application/json']);
        if(!empty($apiResponse) && $apiResponse['status'] == 'ok'){
            $data = [];
            $data['team_a_squad'] = json_encode($apiResponse['response']['teama']);
            $data['team_b_squad'] = json_encode($apiResponse['response']['teamb']);
            $data['teams'] = json_encode($apiResponse['response']['teams']);
            $players = [];
            foreach ($apiResponse['response']['players'] as $pl){
                $players[] = ['pid' => $pl['pid'], 'title' => $pl['title'], 'short_name' => $pl['short_name'], 'thumb_url' => $pl['thumb_url'], 'playing_role' => $pl['playing_role'], 'recent_match' => $pl['recent_match'], 'fantasy_player_rating' => $pl['fantasy_player_rating']];
            }
            $data['players'] = json_encode($players);
            $data['update_at'] = gmdate('Y-m-d H:i:s');
            
            $this->CricketMatches_model->updateDocument($data, ['match_id' => $matchId]);
        }
    }
    
    
    public function competitionMatches($cId){
        $this->loadEntitySportsTraitDependency();
        $env = is_production_env()?'production':'development';
        $entitySportConfig = $this->config->item('entitysport');
        $token = $entitySportConfig['token'][$env];
        
        $url = $entitySportConfig['host']. "competitions/$cId/matches?token=$token&per_page=50&page=1";      // https://rest.entitysport.com/v2/competitions/{mid}/matches?token=
        $apiResponse = curl_request($url, ['Content-Type:application/json']);
        if(!empty($apiResponse) && $apiResponse['status'] == 'ok'){
            
            $matches = $apiResponse['response']['items'];
            echo "\n Count : ".count($matches);
            foreach($matches as $match){
                echo PHP_EOL.$match['match_id'];
                $records = $this->CricketMatches_model->getRecords(['match_id' => $match['match_id']]);
                if(empty($records)){
                    $data = [];
                    $data['match_id'] = $match['match_id'];
                    $data['feeder_name'] = 'entitysport';
                    $data['title'] = $match['title'];
                    $data['short_title'] = $match['short_title'];
                    $data['subtitle'] = $match['subtitle'];
                    $data['status_str'] = strtolower($match['status_str']);
                    $data['date_start'] = $match['date_start'];
                    $data['date_end'] = $match['date_end'];

                    $this->CricketMatches_model->insert($data);
                    
                    
                    $this->updateMatchSquad($match['match_id']);
                }
            }
        }
    }
    
    
    public function checkTeamLineup(){
        $this->loadEntitySportsTraitDependency();
        $matches = $this->CricketMatches_model->getIncomleteLineupMatches();
        foreach($matches as $match){
            if($match['match_id'] == 50219){
                $this->updateMatchSquad($match['match_id']);
                echo PHP_EOL."checkTeamLineup";
            }
            
        }
    }
    
    public function scoreCard($matchId = ''){
        if(empty($matchId))
            return false;
        
        $this->loadEntitySportsTraitDependency();

        $requestedDateTime = new DateTime(gmdate('Y-m-d H:i:s'), new DateTimeZone('UTC'));
        $hasExistsCard = $this->CricketScorecard_model->getRecords(['match_id' => $matchId]);
         
        $apiResponse = []; $matchStartDateTime = 0;
        if(empty($hasExistsCard)){
//            echo "\n Match Not Exists \n";
            $apiResponse = $this->entityScoreCardCurlRequest($matchId);
            if(!empty($apiResponse) && !empty($apiResponse['response'])){
                $this->CricketScorecard_model->insert(['match_id' => $matchId, 'title' => $apiResponse['response']['short_title'], 'match_format' => $apiResponse['response']['format_str'], 'start_date_time' => $apiResponse['response']['date_start'], 'end_date_time' => $apiResponse['response']['date_end']]);
                $matchStartDateTime = $apiResponse['response']['date_start'];
            }else{
                return false;
            }
        }
        else
        {
//            echo "\n Match Exists \n";
            if(!in_array($hasExistsCard[0]['match_status'], ['scheduled', 'live'])){
                return false;
            }
           
            // match time is out from current time
            if(strtotime($requestedDateTime->format('Y-m-d H:i:s')) > strtotime($hasExistsCard[0]['end_date_time']) && (in_array($hasExistsCard[0]['match_status'], ['scheduled', 'live']))){
                
                $apiResponse = $this->entityScoreCardCurlRequest($matchId);
                
//                echo "\n current time is greater than end_date_time and status is - ".strtolower($apiResponse['response']['status_str']). "  \n";
                if(!empty($apiResponse) && !empty($apiResponse['response'])){
                    // 1 => Scheduled , 2 => Completed , 3 => Live , 4 => Abandoned, canceled, no result
                    
                    if( in_array($apiResponse['response']['status'], [1, 3])){
                        if((strtotime($apiResponse['response']['date_end']) > strtotime($hasExistsCard[0]['end_date_time'])))
                            $this->CricketScorecard_model->updateDocument(['match_status' => strtolower($apiResponse['response']['status_str']) ,'end_date_time' => $apiResponse['response']['date_end']], ['id' => $hasExistsCard[0]['id']]);
                    }else{
                        return false;
                    }
                }else{
                    return false;
                }
            }
            $matchStartDateTime = $hasExistsCard[0]['start_date_time'];
        }

        if(!empty($matchStartDateTime) && strtotime($matchStartDateTime) > strtotime($requestedDateTime->format('Y-m-d H:i:s'))){
            return false;
        }
        
        if(empty($apiResponse) || empty($apiResponse['response'])){
            $apiResponse = $this->entityScoreCardCurlRequest($matchId);
        }
//                echo PHP_EOL." VERIFY : "  .$apiResponse['response']['verified']. PHP_EOL;

        $data['match_id'] = $apiResponse['response']['match_id'];
        $data['title'] = $apiResponse['response']['short_title'];
        $data['start_date_time'] = $apiResponse['response']['date_start'];
        $data['end_date_time'] = $apiResponse['response']['date_end'];
        $data['match_format'] = strtolower($apiResponse['response']['format_str']);
        $innings = isset($apiResponse['response']['innings'])?$apiResponse['response']['innings']: null;

        $finalInnings = [];
        if(!empty($innings))
        {
            foreach($innings as $inning){
                echo PHP_EOL.$inning['name']. " Score  : "  .$inning['scores_full'];
                $inn = [];
                $inn['iid'] = $inning['iid'];
                $inn['number'] = $inning['number'];
                $inn['name'] = $inning['name'];
                $inn['scores_full'] = $inning['scores_full'];
                $inn['batsmen'] = $inning['batsmen'];
                $inn['bowlers'] = $inning['bowlers'];
                $inn['fows'] = $inning['fows'];
                $inn['extra_runs'] = $inning['extra_runs'];
                $inn['did_not_bat'] = $inning['did_not_bat'];

                $finalInnings[] = $inn;
            }
            echo PHP_EOL;
        }

        $data['innings'] = json_encode($finalInnings);
        $data['update_at'] = gmdate('Y-m-d H:i:s');
        $data['match_status'] = strtolower($apiResponse['response']['status_str']);
        $condition = !empty($hasExistsCard)?['id' => $hasExistsCard[0]['id']]: ['match_id' => $matchId];
        $this->CricketScorecard_model->updateDocument($data, $condition);
    }
    
    private function entityScoreCardCurlRequest($matchId){
        echo PHP_EOL."-----------------------------------------------------";
        $env = is_production_env()?'production':'development';
        $entitySportConfig = $this->config->item('entitysport');
        $token = $entitySportConfig['token'][$env];
        $scoreCardApi = $entitySportConfig['host']. "matches/$matchId/scorecard?token=$token";      // https://rest.entitysport.com/v2/matches/19899/scorecard?token=
        $start = microtime(true);
        $response = curl_request($scoreCardApi, ['Content-Type:application/json']);
//        print_r($response);
        echo PHP_EOL."Entity Scorecard API Hit Total Time in Seconds : ".(microtime(true) - $start).PHP_EOL;
        return $response;
    }
    

}
