<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkuserlogin_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        if(!is_logged_in()){
            redirect_url('/login');
        }
    }
}
