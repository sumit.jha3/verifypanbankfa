<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'trackContestUserList']); ?>
        <br/>
        <div class="card " style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
                <div class="card" style="">
                    <div class="card-body  col-sm-12 form-control form-inline">
                        <button type="button" class="btn btn-primary col-sm-2 " data-toggle="modal" data-target="#trackcontest_user" > Add User For Track Contest</button>
                        
                    </div>
                </div>
                <br/><br/>
            
           <br/>
            
            <table class="table ">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">id</th>
                    <th scope="col">User Id</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>

                  </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $record){ ?>
                  <tr>
                    <th scope="row"><?= $record['id'] ?></th>
                   

                    <td><?= $record['user_id'] ?></td>
                    <td><b><?= $record['name'] ?></b></td>
                    <td><b><?= $record['status'] ?></b></td>
                    <td> 
                        <input name="" type="checkbox" class="col-sm-1 form_checkbox status_<?= $record['id'] ?>" value="" style="height: 25px; width: 25px;"   <?=  ($record['status']==1)?"checked":"" ?>    />  
                        <button class="btn btn-outline-success form_control  save_status" style="margin-bottom: 15px;width: 25%;" type="button" id="<?= $record['id'] ?>" >Save</button>
                        <br/>
                        <span style="color: green" class="message_show <?= "mess_".$record['id'] ?>"></span>
                    </td>
                  </tr>
                  <?php } ?>

                </tbody>
            </table>
            </div>
        </div>
        
        <!-------------------- Modal for Add new Immediate User For Withdrawal ------------->
        <div class="modal" id="trackcontest_user" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add New User For Contest Track</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="container-fluid">
                        <form>
                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-4 col-form-label">*User Id</label>
                              <div class="col-sm-8"> <input type="number" class="form-control track_userId" id="inputPassword" placeholder="Enter User Id Here"> </div>
                            </div>

                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-4 col-form-label">*Name</label>
                              <div class="col-sm-8"> <input type="text" class="form-control track_user_name" id="inputPassword" placeholder="Enter Name Here"> </div>
                            </div>
                        </form>
                    </div>
              </div>
              <div class="modal-footer">
                <alert class="trackcontest_user_error" style="display: none"></alert>
                <button type="button" class="btn btn-primary save_trackcontest_user">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-------- end modal ---------->
        
        
                
    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/popper.min.js"></script>    
<script src="/application/bootstrap4/bootstrap.min.js"></script>    

<script type="text/javascript">
    $(document).ready(function() {
        $(".save_trackcontest_user").on('click', function(event){

            $('.trackcontest_user_error').html('');
            $('.trackcontest_user_error').css('display',"none");
            
            let userId = $('.track_userId').val();
            let userName = $('.track_user_name').val();
            let u_error = '';
            if(userName=='' || userName == 0 || userName=='undefined'){
                u_error = 'User Name can not be empty';
            }
            if(userId=='' || userId == 0 || userId=='undefined'){
                u_error = 'User Id can not be empty';
            }
            
            if(u_error != ''){
                $('.trackcontest_user_error').html('<b style="color:red" >'+u_error+'</b>');
                $('.trackcontest_user_error').css('display',"block");
                return false;
            }
            
            $.ajax({
                url: "/user/save-trackcontest-user", type: 'POST', data: { 'name': userName, 'user_id': userId, 'type': 'insert'}, dataType  : 'json',
                success: function(res){
                    if(res.success != ''){
                        $('.trackcontest_user_error').html('<b>'+res.success+'</b>');
                        window.location = '/user/track-contest-user-list';
                    }
                    else{
                        $('.trackcontest_user_error').html('<b style="color:red">'+res.error+'</b>');
                    }
                    $('.trackcontest_user_error').css('display',"block");
                }
            });
        });
        
        
        $(".save_status").on('click', function(event){
            var id = $(this).attr('id');
            let status = ($('.status_'+id).prop('checked')==true)?1:0;
            
            $('.message_show').html('');
            
            $.ajax({
                url: "/user/save-trackcontest-user", type: 'POST', data: { 'id': id, 'type': 'update', 'status': status}, dataType  : 'json',
                success: function(res){
                    if(res.success != ''){
                       $('.mess_'+id).html('Successfully Updated');
                    }
                    else{
                        $('.mess_'+id).html('Failed. Please try again');
                    }
                }
            });
        });
    });
</script>

</html>