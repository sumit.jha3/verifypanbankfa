<ul class="nav nav-tabs" style="margin-top: 10px;">
    
    <li class="nav-item">
      <a class="nav-link <?php echo ($active =='user_dashboard'? 'active': ''); ?>" href="/user/dashboard/">User Pan & Bank</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  <?php echo ($active =='withdrawal'? 'active': ''); ?> " href="/transaction/withdrawal/">Withdrawal</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  <?php echo ($active =='immediate_user'? 'active': ''); ?> " href="/immediate-user/index/0">ImmediateUser Withdrawal</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  <?php echo ($active =='trackContestUserList'? 'active': ''); ?> " href="/user/track-contest-user-list">Track Contest User List</a>
    </li>
    <li class="nav-item">
      <a class="nav-link  <?php echo ($active =='settings'? 'active': ''); ?> " href="/settings">Settings</a>
    </li>
     
</ul>

