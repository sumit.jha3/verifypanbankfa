<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'withdrawal']); ?>
        <br/>
        <div class="card " style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
            <div class="row">
                <i style="display: none;margin-left: 90%;margin-bottom: 9px;" class="pending_withdrawal_fa_icon fa fa-spinner fa-3x fa-spin"></i>
                <div class="col-sm-8">
                    <div class="btn-group">
                        <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Filter By - Status <?php echo !empty($status)? " - $status":""; ?>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                          <a class="dropdown-item" href="/transaction/withdrawal?status=all">All</a>
                          <a class="dropdown-item" href="/transaction/withdrawal?status=success">Success</a>
                          <a class="dropdown-item" href="/transaction/withdrawal?status=pending">Pending</a>
                          <a class="dropdown-item" href="/transaction/withdrawal?status=error">Error</a>
                        </div>
                    </div>
                </div>
                                
                <div class="col-sm-4 input-group " >
                    <input type="number" name="amount_limit" class="amount_limit form-control" placeholder=" Amount Limit" />
                    <div class="input-group-append">
                        <button type="button" class="btn btn-warning process_pending_withdrawal form-control" style="">Process Pending Withdrawal From FA</button>
                    </div>
                    <span style="color: red" class="withdrawal_process_error"></span>
                </div>
            </div>
           <br/>
            
            <table class="table ">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">id</th>
                    <th scope="col">User<br/>unique id</th>

                    <th scope="col">Beneficiary<br/>Id</th>
                    <th scope="col">Rs.</th>
                    <th scope="col">TransferId /<br/> <b>LatestTransferId</b> </th>
                    <th scope="col">Reference<br/>Id</th>
                    <th scope="col">Bank Account/<br/>IFSC Code</th>

                    <th scope="col">Message</th>
                    <th scope="col">Status/<br/> Code</th>
                    <th scope="col">Process<br/>Attempt</th>
                    <th scope="col">UserName/<br/><b>FullName</b></th>
                    <th scope="col">Created</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $record){ ?>
                  <tr>
                    <th scope="row"><?= $record['id'] ?></th>
                    <td><?= $record['user_unique_id'] ?></td>

                    <td><?= $record['beneId'] ?></td>
                    <td><b><?= $record['amount'] ?></b></td>
                    <td><?= $record['transferId'] ?><br/><b><?= $record['latest_transferId'] ?></b></td>
                    <td><?= $record['referenceId'] ?></td>
                    <td><b><?= $record['bankAccount'] ?></b><br/><?= $record['ifsc'] ?></td>
                    
                    <td><?= $record['message'] ?></td>
                    <td><b><?= $record['status'] ?></b><br/><?= $record['status_code'] ?></td>
                   
                    

                    <td><?= $record['process_attempt'] ?></td>
                    <td><?= $record['user_name'] ?> <br/> <b><?= $record['full_name'] ?></b></td>
                    <td><?= date("Y-m-d H:i:s",strtotime($record['created_at'])) ?></td>
                    
                  </tr>
                  <?php } ?>

                </tbody>
            </table>
                
                
                
            </div>
        </div>
        
        
        
                
    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/popper.min.js"></script>    
<script src="/application/bootstrap4/bootstrap.min.js"></script>    

<script type="text/javascript">
    $(document).ready(function() {
        $(".process_pending_withdrawal").on('click', function(event){
            $('.withdrawal_process_error').html('');
            var amount_limit = $('.amount_limit').val();
            if(amount_limit < 1){
                $('.withdrawal_process_error').html('Please Enter Valid Amount Limit. Request will process withdrawal under your mentioned amount limit.');
                return false;
            }
            $(".amount_limit").css('display',"none");
            $('.pending_withdrawal_fa_icon').css('display',"block");
            $(".process_pending_withdrawal").css('display',"none");
            
            $.ajax({
                url: "/transaction/pending-withdrawal-processing", type: 'POST', data: {'process': true, 'amount_limit': amount_limit}, dataType  : 'json',
                success: function(res){
                    if(res.error != ''){
                        $('.withdrawal_process_error').html(res.error);
                        $('.pending_withdrawal_fa_icon').css('display',"none");
                        $(".process_pending_withdrawal").css('display',"block");
                        $(".amount_limit").css('display',"block");
                    }
                    else{
                        $('.amount_limit').val('');
                        $('.pending_withdrawal_fa_icon').css('display',"none");
                        $(".process_pending_withdrawal").css('display',"block");
                        $(".amount_limit").css('display',"block");
                        window.location.href = '/transaction/withdrawal';
                    }
                }
            });
        });
    });
</script>

</html>