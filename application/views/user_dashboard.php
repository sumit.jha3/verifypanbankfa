<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        
        <style >
            .numlink,  .curlink, .firstlink, .lastlink {
                padding: 5px;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'user_dashboard']); ?>
        
        <br/>
        <div class="card" style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
            <nav class="navbar navbar-light bg-light">
                
                <form class="form-inline">
                    <input style="" class="form-control mr-sm-4 ifsc_code_input" type="input" placeholder="Add new IFSC Code " aria-label="Ifsc">
                    <button class="btn btn-outline-success my-2 my-sm-1 add_ifsc_code" type="button">Insert</button>
                    <i style="display: none;margin-right: 76px;" class="add_ifsc_process_icon fa fa-spinner fa-3x fa-spin"></i>
                    <span style="color: red" class="ifsccode_error"></span>
                    <span style="color: green" class="ifsccode_sucess"></span>
                </form>
                
                <div class="">
                    
                    <div class="col-sm-7"></div>
                    
                    <div class="col-sm-5 btn-group" >
                        <input type="button" value="Reprocess Failed Bank" style="padding: 10px;margin-right: 10px;" class="btn btn-info"  onclick="location.href='/user/pan-bank-reprocess'; " /> 
                        <button type="button" class="btn btn-warning process_pan_data" style="padding: 10px;">Process Pending PAN & Bank Data From FA</button>
                        <i style="display: none;margin-right: 76px;" class="processing_fa_icom fa fa-spinner fa-3x fa-spin"></i>
                        <span style="color: red" class="panbank_process_error"></span>
                    </div>
                </div>
            </nav>
            <br/>    
                

           
            <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">id</th>
                    <th scope="col">user<br/>id</th>
                    <th scope="col">user<br/>uniqueId/<br/>Name</th>
                   
                    <th scope="col"><b>Pan processed</b>/<br/>Bank processed</th>
                    
                    <th scope="col"><b>pan verified</b>/ <br/> Bank verified </th>

                    <th scope="col">pan<br/>no</th>
                    <th scope="col">pan<br/>rejected<br/>reason</th>
                    
                    <th scope="col">Bank Account Number/ <br/><b>IFSC Code</b></th>
                    <th scope="col">Bank<br/>Rejected<br/>reason</th>
                    <th scope="col">Signup<br/>Date</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($user as $userdetail){ ?>
                  <tr class="<?php echo (empty($userdetail['is_processed']))?'table-danger': (($userdetail['pan_verified'] && $userdetail['bank_verified'])?'table-success':'') ?>">
                    <th scope="row"><?= $userdetail['id'] ?></th>
                    <td><?= $userdetail['user_id'] ?></td>
                    <td> <?= $userdetail['user_unique_id'] ?> <br/><b> <?= $userdetail['first_name'] ?> </b></td>
                    <td><b><?= $userdetail['pan_processed'] ?></b> <br/> <?= $userdetail['bank_processed'] ?></td>
                
                    <td><b><?= $userdetail['pan_verified'] ?></b> <br/> <?= $userdetail['bank_verified'] ?> </td>
                    <td><?= $userdetail['pan_no'] ?></td>
                    <td><?= $userdetail['pan_rejected_reason'] ?></td>
                    
                    <td><?= $userdetail['bank_number'] ?> <br/> <b><?= $userdetail['bank_ifsc_code'] ?></b></td>
                    <td><?= $userdetail['bank_rejected_reason'] ?></td>
                    
                    <td><?= date("Y-m-d",strtotime($userdetail['added_date'])) ?></td>
                    
                  </tr>
                  <?php } ?>

                </tbody>
            </table>
            
                <div class="pagination col-sm-12 " style="margin: 40px;">
                    <div class=" col-sm-4 "></div> <?php echo $this->pagination->create_links(); ?>
                </div> 
            </div>
        </div>

    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/bootstrap.min.js"></script>    



<script type="text/javascript">
    $(document).ready(function() {
        $(".process_pan_data").on('click', function(event){
            $('.processing_fa_icom').css('display',"block");
            $(".process_pan_data").css('display',"none");
            $('.panbank_process_error').html('');
            $.ajax({
                url: "/user/process_pan_data_from_fa", type: 'POST', data: {'process': true}, dataType  : 'json',
                success: function(res){
                    if(res.error != ''){
                        $('.panbank_process_error').html(res.error);
                        $('.processing_fa_icom').css('display',"none");
                        $(".process_pan_data").css('display',"block");
                    }
                    else{
                        $('.processing_fa_icom').css('display',"none");
                        $(".process_pan_data").css('display',"block");
                        window.location.href = '/user/dashboard';
                    }
                }
            });
        });
        
        $(".add_ifsc_code").on('click', function(event){
            
            var ifsc_codes = $('.ifsc_code_input').val();
            if(ifsc_codes==''){
                $('.ifsccode_error').html('Ifsc code not empty');
                return false;
            }
            $('.ifsccode_error').html('');$('.ifsccode_sucess').html('');
            $('.add_ifsc_process_icon').css('display',"block");
            $(".add_ifsc_code").css('display',"none");
            $.ajax({
                url: "/user/add_ifsc_code", type: 'POST', data: {'ifsc_codes': ifsc_codes}, dataType  : 'json',
                success: function(res){
                    if(res.error!=''){
                        $('.ifsccode_error').html(res.error);
                    }
                    if(res.success!=''){
                        $('.ifsccode_sucess').html(res.success);
                    }
                    $('.add_ifsc_process_icon').css('display',"none");
                    $(".add_ifsc_code").css('display',"block"); 

//                    window.location.href = '/user/dashboard';
                },
                error: function(req, err){  }
            });
        });
    });
</script>
</html>