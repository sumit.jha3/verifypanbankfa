<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'settings']); ?>
        
        <br/>
        <div class="card" style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
                
                <?php if(!empty($settings) && is_array($settings)){  ?>
                    <?php foreach($settings as $settingDetail){  ?>
                        <nav class="navbar navbar-light bg-light">
                            <form   class="form-inline col-sm-12">
                                <a class="navbar-brand col-sm-4"><?=  $settingDetail['setting_name'] ?></a>
                                <input style="" class="form-control col-sm-2 <?=  "value_".$settingDetail['setting_name'] ?>" type="input" value="<?=  $settingDetail['setting_value'] ?>"  >
                                <a class="navbar-brand col-sm-1"> </a>
                                
                                <input name="" type="checkbox" class="col-sm-1 form_checkbox <?=  "status_".$settingDetail['setting_name'] ?>" value="<?=  $settingDetail['status'] ?>" style="height: 25px; width: 25px;" <?=  ($settingDetail['status']==1)?"checked":"" ?> />  <a  class="navbar-brand"> Status</a>
                                <a class="navbar-brand col-sm-1"> </a>
                                
                                <button class="btn btn-outline-success col-sm-1 save_setting" type="button" id="<?= $settingDetail['id'] ?>" setting_name="<?= $settingDetail['setting_name'] ?>">Save</button>
                                
                                <i style="display: none;" class="add_ifsc_process_icon fa fa-spinner fa-3x fa-spin <?= "spin_".$settingDetail['setting_name'] ?> "></i>
                                <span style="color: red" class="<?= "error_".$settingDetail['setting_name'] ?>"></span>
                                <span style="color: orange" class="<?= "sucess_".$settingDetail['setting_name'] ?>"></span>
                                
                            </form>
                        </nav>
                        <br/>
                    <?php }  ?>
                <?php }else{ echo "<h1>No Settings is saved</h1>"; } ?>
            </div>
        </div>


    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/bootstrap.min.js"></script>    



<script type="text/javascript">
    $(document).ready(function() {
        
         $(".form_checkbox").on('click', function(event){
            var status = $(this).attr('value');
            if(status == 1){
                $(this).attr('value', 0);
            }else if (status == 0){
                $(this).attr('value', 1);
            } 
         });
        
        $(".save_setting").on('click', function(event){
            var setting_name = $(this).attr('setting_name');
            var id = $(this).attr('id');
            let value = $('.value_'+setting_name).val();
            let status = $('.status_'+setting_name).val();
            
            if(value == ""){
                $('.error_'+setting_name).html('Setting '+setting_name + ' value is not valid');
                return;
            }else{
                $('.error_'+setting_name).html('');
            }
            
//            console.log(id , setting_name, value, status);
            $('#'+id).css('display',"none");
            $('.spin_'+setting_name).css('display',"block");
            $('.error_'+setting_name).html('');
            $('.sucess_'+setting_name).html('');
            $.ajax({
                url: "/settings/save", type: 'POST', data: {'setting_name': setting_name, 'setting_value':value, 'id': id, 'status': status}, dataType  : 'json',
                success: function(res){
                    if(res.error!=""){
                        $('.error_'+setting_name).html(res.error);
                    }else{
                        $('.sucess_'+setting_name).html(res.success);
                    }
                    $('#'+id).css('display',"block");
                    $('.spin_'+setting_name).css('display',"none");
                }
            });
        });
    });
</script>
</html>