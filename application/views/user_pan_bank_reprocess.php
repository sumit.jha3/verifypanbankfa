<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        
        <style >
            .numlink,  .curlink, .firstlink, .lastlink {
                padding: 5px;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'user_dashboard']); ?>
        
        <br/>
        <div class="card" style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
            
            <nav class="navbar navbar-light bg-light">
                
                <div class="">
                  
                    <div class=" btn-group " >
                        <button type="button" class="btn btn-success approve_selected_pan_bank" style="padding: 10px;">Approve Selected Bank</button>
                        <button type="button" class="btn btn-warning reject_selected_pan_bank " data-toggle="modal" data-target="#reject_pan_bank_Modal" style="padding: 10px;margin-left: 10px;">Reject Selected Bank</button>
                        <button type="button" class="btn btn-danger delete_selected_pan_bank "  style="padding: 10px;margin-left: 10px;">Delete Selected Record</button>
                        <i style="display: none;" class="processing_fa_icon fa fa-spinner fa-3x fa-spin"></i>
                    </div>
                    <span style="color: red" class="panbank_reprocess_error"></span>
                </div>
                
                
                
            </nav>
            <br/> 
           
            <table class="table">
                <thead class="thead-light">
                  <tr>
                    <th scope="col"></th>
                    <th scope="col">id</th>
                    <th scope="col">user<br/>id</th>
                    <th scope="col">user<br/>uniqueId/<br/>Name</th>
                    <th scope="col">Name in PAN/<br/>Name in Bank</th>
                    
                    <th scope="col">Bank<br/>Rejected<br/>reason</th>
                    <th scope="col">Signup<br/>Date</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($user as $userdetail){   
                        $extra_data = json_decode($userdetail['extra_data'], true);
                        $userNameInPan = $userNameInBank = '';
                        if(!empty($extra_data) && !empty($extra_data['bank_process'])){
                            $userNameInPan = !empty($extra_data['bank_process']['pan_name'])?$extra_data['bank_process']['pan_name']: '-';
                            $userNameInBank = !empty($extra_data['bank_process']['user_name_in_bank'])?$extra_data['bank_process']['user_name_in_bank']:'-';
                        }
                        ?>
                        <tr>
                            <td><input class="dbId_checkbox" style="width: 25px;height: 23px;" type="checkbox" value="<?= $userdetail['id'] ?>" /></td>
                            <td scope="row"><?= $userdetail['id'] ?></td>
                            <td><?= $userdetail['user_id'] ?></td>
                            <td> <?= $userdetail['user_unique_id'] ?> <br/> <b> <?= $userdetail['first_name'] ?> </b></td>
                            
                            <td> <?= $userNameInPan ?> <br/> <b> <?= $userNameInBank ?> </b></td>
                            <td><?= $userdetail['bank_rejected_reason'] ?></td>
                            <td><?= date("Y-m-d",strtotime($userdetail['added_date'])) ?></td>
                        </tr>
                    <?php } ?>

                </tbody>
            </table>
            
            </div>
        </div>

        <!-------------------- Modal for Reject Bank Verification ------------->
        <div class="modal" id="reject_pan_bank_Modal" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Reject Bank Verification</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="container-fluid">
                        <form>
                            <div class="form-group row">
                              <label for="inputPassword" class=" col-form-label">*Rejected Message</label>
                            </div>
                            <div class="form-group row">
                                <div class=""> <textarea style="height: 150px;width:450px" class="reject_msg"></textarea> </div>
                            </div>
                            <span style="color: red" class="reject_error"></span>
                          
                        </form>
                    </div>
              </div>
              <div class="modal-footer">
                <alert class="immediate_user_error" style="display: none"></alert>
                <button type="button" class="btn btn-primary reject_bank_verification">Submit</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-------- end modal ---------->
        
        
    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/bootstrap.min.js"></script>    



<script type="text/javascript">
    $(document).ready(function() {
        $('.approve_selected_pan_bank').on('click', function(){
            $(".processing_fa_icon").css('display',"block");
            $(".approve_selected_pan_bank").css('display',"none");
            $('.panbank_reprocess_error').html('');
            var ids = [];
            $('.dbId_checkbox').each(function () {
               if (this.checked) {
                   ids.push($(this).val());
               }
            });
            if(ids.length === 0){
                $('.panbank_reprocess_error').html('Select Atleast one record to reprocess.');
                $(".processing_fa_icon").css('display',"none");
                $(".approve_selected_pan_bank").css('display',"block");
                return false
            }
            $.ajax({
                url: "/user/pan-bank-reprocess-submit", type: 'POST', data: {'ids': ids}, dataType  : 'json',
                success: function(res){
                    if(res.status == false){
                        $('.panbank_reprocess_error').html('Error, Please try again');
                        $(".processing_fa_icon").css('display',"none");
                        $(".approve_selected_pan_bank").css('display',"block");
                    }else{
                        window.location.href = '/user/pan-bank-reprocess';
                    }
                }
            });
        });
        
        $('.reject_selected_pan_bank').on('click', function(){
            var ids = [];
            $('.panbank_reprocess_error').html('');
            $('.dbId_checkbox').each(function () {
               if (this.checked) {
                   ids.push($(this).val());
               }
            });
            if(ids.length === 0){
                $('.panbank_reprocess_error').html('Select Atleast one record to reprocess.');
                return false
            }
        });
        
        $('.reject_bank_verification').on('click', function(){
            $('.reject_error').html('');
            var msg = $('.reject_msg').val();
            if(msg =='' ||  msg == 'undefined'){
                $('.reject_error').html('Message can not be empty');
                return false;
            }
            
            var ids = [];
            $('.dbId_checkbox').each(function () {
               if (this.checked) { ids.push($(this).val()); }
            });
            if(ids.length === 0){ $('.reject_error').html('Select Atleast one record to reprocess.'); return false; }
            
            $.ajax({
                url: "/user/pan-bank-reprocess-submit", type: 'POST', data: {'ids': ids, 'message': msg, 'rejected': 1}, dataType  : 'json',
                success: function(res){
                    if(res.status == false){
                        $('.reject_error').html('Error, Please try again');
                    }else{
                        window.location.href = '/user/pan-bank-reprocess';
                    }
                }
            });
        });
        
        
        $('.delete_selected_pan_bank').on('click', function(){
            
            $(".processing_fa_icon").css('display',"block");
            $(".delete_selected_pan_bank").css('display',"none");
            var ids = [];
            $('.dbId_checkbox').each(function () {
               if (this.checked) { ids.push($(this).val()); }
            });
            if(ids.length === 0)
            { $('.panbank_reprocess_error').html('Select Atleast one record to process.'); 
                $(".processing_fa_icon").css('display',"none");
                $(".delete_selected_pan_bank").css('display',"block");
                return false; 
            }
            
            $.ajax({
                url: "/user/pan-bank-reprocess-submit", type: 'POST', data: {'ids': ids, 'deleted': 1}, dataType  : 'json',
                success: function(res){
                    if(res.status == false){
                        $(".processing_fa_icon").css('display',"none");
                        $(".delete_selected_pan_bank").css('display',"block");  
                        $('.panbank_reprocess_error').html('Error, Please try again');
                    }else{
                        window.location.href = '/user/pan-bank-reprocess';
                    }
                }
            });
        });
        
        
    });
</script>
</html>