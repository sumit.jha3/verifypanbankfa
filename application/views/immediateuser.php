<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">
	<title>Fantasy Akhada</title>
        <link rel="stylesheet" href="/application/bootstrap4/bootstrap.min.css" >
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
        <style >
            .numlink,  .curlink, .firstlink, .lastlink {
                padding: 5px;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        
        <?php $this->load->view('headertab', ['active' => 'immediate_user']); ?>
        <br/>
        <div class="card " style="margin-left: 10px;margin-right: 10px;">
            <div class="card-body">
                <div class="card" style="">
                    <div class="card-body  col-sm-12 form-control form-inline">
                        <button type="button" class="btn btn-primary col-sm-2 " data-toggle="modal" data-target="#immediate_user" > Add Immediate User</button>
                        <div class="col-sm-2"></div>
                        <div class="col-sm-2"> OR</div>
                        <div class="col-sm-6  alert-primary" style="padding: .5rem 01rem;">
                            <?php if($this->input->get('for') =='uiuc' && $this->input->get('m') =='s'){ ?>
                            <span class="d-block p-2 bg-dark text-white">CSV Successfully Uploaded.</span>
                            <?php } ?>
                            <form class="form-horizontal border border-danger" style="border: 1px solid #dee2e6!important;" action="/immediate-user/upload-immediate-user-csv" method="POST" name="" id="frmCSVImport" enctype="multipart/form-data">
                                <div class="input-row" style="margin: 5px;">
                                    <input type="file" name="file" id="file" accept=".csv" />
                                    <button type="submit" id="submit"  name="immediateUserImp" value="1" class="btn btn-submit  btn-warning">Upload Immediate User CSV</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <br/><br/>
            
           <br/>
            
            <table class="table ">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">id</th>
                    <th scope="col">User unique id</th>
                    <th scope="col">User Id</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Status</th>

                  </tr>
                </thead>
                <tbody>
                    <?php foreach($records as $record){ ?>
                  <tr>
                    <th scope="row"><?= $record['id'] ?></th>
                    <td><?= $record['user_unique_id'] ?></td>

                    <td><?= $record['user_id'] ?></td>
                    <td><b><?= $record['name'] ?></b></td>
                    <td><b><?= $record['status'] ?></b></td>
                  </tr>
                  <?php } ?>

                </tbody>
            </table>
               
                <div class="pagination col-sm-12 " style="margin: 40px;">
                <div class=" col-sm-4 "></div>
                    <?php echo $this->pagination->create_links(); ?>
                </div> 
            </div>
        </div>
        
        <!-------------------- Modal for Add new Immediate User For Withdrawal ------------->
        <div class="modal" id="immediate_user" tabindex="-1" role="dialog">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Add New Immediate User For Withdrawal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                    <div class="container-fluid">
                        <form>
                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-4 col-form-label">*User Id</label>
                              <div class="col-sm-8"> <input type="number" class="form-control immediate_userId" id="inputPassword" placeholder="Enter User Id Here"> </div>
                            </div>

                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-4 col-form-label">*User Unique Id</label>
                              <div class="col-sm-8"> <input type="text" class="form-control immediate_user_uniqueId" id="inputPassword" placeholder="Enter User Unique Id Here"> </div>
                            </div>

                            <div class="form-group row">
                              <label for="inputPassword" class="col-sm-4 col-form-label">*Name</label>
                              <div class="col-sm-8"> <input type="text" class="form-control immediate_user_name" id="inputPassword" placeholder="Enter Name Here"> </div>
                            </div>
                        </form>
                    </div>
              </div>
              <div class="modal-footer">
                <alert class="immediate_user_error" style="display: none"></alert>
                <button type="button" class="btn btn-primary save_immediate_user">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              </div>
            </div>
          </div>
        </div>
        <!-------- end modal ---------->
        
        
                
    </body>

<script src="/application/bootstrap4/jquery2.1.min.js"></script>
<script src="/application/bootstrap4/popper.min.js"></script>    
<script src="/application/bootstrap4/bootstrap.min.js"></script>    

<script type="text/javascript">
    $(document).ready(function() {
        $(".save_immediate_user").on('click', function(event){

            $('.immediate_user_error').html('');
            $('.immediate_user_error').css('display',"none");
            
            let userId = $('.immediate_userId').val();
            let userUniqueId = $('.immediate_user_uniqueId').val();
            let userName = $('.immediate_user_name').val();
            let u_error = '';
            if(userName=='' || userName == 0 || userName=='undefined'){
                u_error = 'User Name can not be empty';
            }
            if(userUniqueId=='' || userUniqueId == 0 || userUniqueId=='undefined'){
                u_error = 'User Unique Id can not be empty';
            }
            if(userId=='' || userId == 0 || userId=='undefined'){
                u_error = 'User Id can not be empty';
            }
            
            if(u_error != ''){
                $('.immediate_user_error').html('<b style="color:red" >'+u_error+'</b>');
                $('.immediate_user_error').css('display',"block");
                return false;
            }
            
            $.ajax({
                url: "/immediate-user/save-immediate-user", type: 'POST', data: { 'name': userName, 'user_unique_id': userUniqueId, 'user_id': userId}, dataType  : 'json',
                success: function(res){
                    if(res.success != ''){
                        $('.immediate_user_error').html('<b>'+res.success+'</b>');
                    }
                    else{
                        $('.immediate_user_error').html('<b style="color:red">'+res.error+'</b>');
                    }
                    $('.immediate_user_error').css('display',"block");
                }
            });
            
            
            
        });
    });
</script>

</html>