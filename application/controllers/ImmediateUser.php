<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once(APPPATH."core/Checkuserlogin_Controller.php");

class ImmediateUser extends Checkuserlogin_Controller {


    public function __construct() {
        parent::__construct();
        $this->load->model('ImmediateUserWithdrawal_model');
    }
    
    public function index()
    {
        $size = 20;
        $totalCount = $this->ImmediateUserWithdrawal_model->getAllRecords(['totalCount' => true]);
        $offset =  pagination('/immediate-user/index', $size, $totalCount);
               
        $records = $this->ImmediateUserWithdrawal_model->getAllRecords(['offset' => $offset, 'size' => $size]);
        $this->load->view('immediateuser', ['records' => $records]);
    }
    
    

    public function saveImmediateUser(){
        if(empty($this->input->post('name')) || empty($this->input->post('user_unique_id')) || empty($this->input->post('user_id')))
        {
            echo json_encode(['error' => "Invalid Request", 'success' => '']);
            return false;
        }
        
        $response = $this->ImmediateUserWithdrawal_model->insert(['name' => $this->input->post('name'), 'user_id' => $this->input->post('user_id'), 'user_unique_id' => $this->input->post('user_unique_id')]);
        if($response){
            echo json_encode(['success' => "User is successfully Added.", 'error' => '']);
        }else{
             echo json_encode(['error' => "Something went wrong, Please try after some Time.", 'success' => '']);
        }
        return true;
        
    }
    
    public function uploadImmediateUserCSV(){
        if (isset($_POST["immediateUserImp"])) {
    
            $fileName = $_FILES["file"]["tmp_name"];

            if ($_FILES["file"]["size"] > 0) {

                $file = fopen($fileName, "r");
                $sqlInsert = "INSERT ignore into users ";
                $counter = 1;
                $values = $head = $query = "";
                while (($column = fgetcsv($file, 10000, ",")) !== FALSE) {
                   
                    $first = isset($column[0])?$this->db->escape_str($column[0]): "";
                    $second = isset($column[1])?$this->db->escape_str($column[1]): "";
                    $third = isset($column[2])?$this->db->escape_str($column[2]): "";
                    if(empty($first) || empty($second) || empty($third)){
                        continue;
                    }
                    // check header is fixed or not.
                    if($counter == 1 && !((in_array($first, ['user_id', 'user_unique_id', 'name'])) && in_array($second, ['user_id', 'user_unique_id', 'name']) && in_array($third, ['user_id', 'user_unique_id', 'name']))){
                        break;
                    }
                       
                    if($counter == 1){
                        $head .= " ($first, $second, $third) ";
                    }else{
                        $values .= " ($first,'$second','$third'),";
                    }
                    
                    $counter++;
                }
                if(!empty($head) && !empty($values)){
                    $values = rtrim($values, ",");
                    $query = "INSERT IGNORE INTO immediate_user_withdrawal ". $head ." VALUES ". $values.";";
                    $result = $this->ImmediateUserWithdrawal_model->rawQueryExecute($query);
                    if($result){
                        redirect_url('/immediate-user/index?m=s&for=uiuc');
                    }
                }
            }
        }
    }
}
