<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
    }

    public function index()
    {
        if(is_logged_in()){
            redirect_url('/user/dashboard');
        }
        $this->load->view('login_form');
    }
    
    public function check()
    {
        if(is_logged_in()){
            redirect_url('/user/dashboard');
        }
        
        $this->load->model('employee_model');
        $this->form_validation->set_rules('username', 'Username', 'required|min_length[5]');
        $this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('login_form');
        }
        else
        {
            $result = $this->employee_model->checkEmployeeExists($this->input->post('username'), $this->input->post('password'));
            if(empty($result)){
                $this->load->view('login_form', ['error' => "Invalid Credential/User not found."]);
            }
            else if($result[0]['status'] == 0){
                $this->load->view('login_form', ['error' => "User Is Inactive"]);
            }
            else{
                $data_session_set = array('username' => $result[0]['username'], 'user_id' => $result[0]['id']);
                $this->session->set_userdata("user_session", $data_session_set);
                redirect_url('/user/dashboard');
            }
        }

    }
}
