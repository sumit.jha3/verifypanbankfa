<?php

require_once(APPPATH."libraries/UserPanBankTrait.php");
require_once(APPPATH."libraries/UserReferralTrait.php");
require_once(APPPATH."libraries/CommonTrait.php");

class Tools extends CI_Controller {

    use UserPanBankTrait, CommonTrait;
    use UserReferralTrait;
        
    function __construct() {
        parent::__construct();
        if(!is_cli())
            die('This can be access from Command line.');
    }
    
    function checkprocessedWithdrawalData($for = ''){
        
        $for = empty($for)? 'all': $for;
        
        $this->load->helper('cashfree_helper');
        if(in_array($for, [ 'pending', 'success' ]))
        {
            cashfree_reprocess_withdrawal($for);
        }
    }
    
    function processImmediateUserWithdrawal(){
        $this->load->helper('cashfree_helper');
        pendingWithdrawalProcessingHelper('process_only_immediate_user');
    }
    
    
    function processPendingWithdrawalFromFa(){
        $this->load->helper('cashfree_helper');
        $this->load->model('Settings_model');
        
        $setting = $this->Settings_model->getSettingByName('pending_withdrawal_cron');
        if(!empty($setting) && ($setting['status'] == 0)){
            echo PHP_EOL."Pending Withdrawal Cron Setting is Inactive.".PHP_EOL;
            return false;
        }
        echo PHP_EOL."Process Pending Withdrawal".PHP_EOL;
        $this->checkprocessedWithdrawalData('pending');
        sleep(30);
        echo PHP_EOL."Process All Withdrawal".PHP_EOL;
        pendingWithdrawalProcessingHelper('all');
    }
    
    function processPanBankDataFromFA(){
        $this->processPanBankDataFromFaTrait();
    }
    
    function referralAmountDistribution(){
        set_time_limit(600);
        $this->usersReferral();
    }
    
    function trackUserContest(){
        $this->findUserContest();
    }
}


?>