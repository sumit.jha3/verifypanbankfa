<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once(APPPATH."core/Checkuserlogin_Controller.php");
require_once(APPPATH."libraries/EntitySportsTrait.php");

class EntitySports extends Checkuserlogin_Controller {

    use EntitySportsTrait;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('FaUserPanBank_model');
    }
    
    public function scorecard($matchId){
        if(empty($matchId))
            return false;
        
        $this->load->model('CricketMatches_model');
        $this->load->model('CricketScorecard_model');
        
        $matches = $this->CricketScorecard_model->getRecords(['match_id' => $matchId]);
        if(!empty($matches)){
            $match = $matches[0];
            echo "<br/><center><h1> Match Between - ".$match['title']."</h1></center>";
            echo "<center style='color:green'><b>Status: ".$match['match_status']."</b></center><br/>";
            $innings = $match['innings'];
            $innings = !empty($innings)? json_decode($innings, true): [];
            $this->table($innings);
        }
    }
    
    
    private function table($innings){
        
        
        $players = [];
        foreach($innings as $inning){
           
            $batsmen = $inning['batsmen'];
        $html = "<html><head>  <link rel='stylesheet' href='/application/bootstrap4/bootstrap.min.css' > </head>";    
        $html .= "<body><div><h1>".$inning['name']." - ".$inning['scores_full']. "</h1>";    
        $html .= '<table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Batting</th>
                        <th scope="col">R</th>
                        <th scope="col">B</th>
                        <th scope="col">4s</th>
                        <th scope="col">6s</th>
                        <th scope="col">S/R</th>
                      </tr>
                    </thead>
                    <tbody>';
                    foreach($batsmen as $batting){
                        $html .=  '<tr>
                                    <td ><b>'.$batting['name'].'</b><br>'.$batting['how_out'].'</td>
                                    <td>'.$batting['runs'].'</td>
                                    <td>'.$batting['balls_faced'].'</td>
                                    <td>'.$batting['fours'].'</td>
                                    <td>'.$batting['sixes'].'</td>
                                    <td>'.$batting['strike_rate'].'</td>
                                  </tr>';
                    }
                    $html .='</tbody>
                </table> </div> <br/><br/>';
                    if(!empty($inning['did_not_bat'])){
                        $html .= '<b>Yet To Bat :  </b>'; 
                        foreach ($inning['did_not_bat'] as $m){
                            $html .= $m['name'].", ";
                        }
                        $html .= "<br/>";
                    }

                    $html .= '<br/><table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Bowling</th>
                        <th scope="col">O</th>
                        <th scope="col">M</th>
                        <th scope="col">R</th>
                        <th scope="col">W</th>
                        <th scope="col">Econ</th>
                      </tr>
                    </thead>
                    <tbody>';
                    foreach($inning['bowlers'] as $bow){
                        $html .=  '<tr>
                                    <td ><b>'.$bow['name'].'<b></td>
                                    <td>'.$bow['overs'].'</td>
                                    <td>'.$bow['maidens'].'</td>
                                    <td>'.$bow['runs_conceded'].'</td>
                                    <td>'.$bow['wickets'].'</td>
                                    <td>'.$bow['econ'].'</td>
                                  </tr>';
                    }


                 $html .='</tbody> </table> </div> <br/><br/><hr/><hr/><br/><br/> </body></html>';
                echo $html;
                $html = '';



                foreach( $inning['batsmen'] as $p){
                    $players[$p['batsman_id']]['name'] = $p['name'];
                    $players[$p['batsman_id']]['runs'] = $p['runs'];
                    $players[$p['batsman_id']]['balls_faced'] = $p['balls_faced'];
                    $players[$p['batsman_id']]['fours'] = $p['fours'];
                    $players[$p['batsman_id']]['sixes'] = $p['sixes'];
                    $players[$p['batsman_id']]['strike_rate'] = $p['strike_rate'];
                }

                foreach($inning['bowlers'] as $b){
                    $players[$b['bowler_id']]['overs'] = $b['overs'];
                    $players[$b['bowler_id']]['name'] = $b['name'];
                    $players[$b['bowler_id']]['maidens'] = $b['maidens'];
                    $players[$b['bowler_id']]['runs_conceded'] = $b['runs_conceded'];
                    $players[$b['bowler_id']]['wickets'] = $b['wickets'];
                    $players[$b['bowler_id']]['econ'] = $b['econ'];
                }
        
            
        }
        
        
              
    $html = "<html><head>  <link rel='stylesheet' href='/application/bootstrap4/bootstrap.min.css' > </head>";    
    $html .= "<body><div><h1>Player Stats</h1>";    
    $html .= '<table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">R</th>
                    <th scope="col">B</th>
                    <th scope="col">4s</th>
                    <th scope="col">6s</th>
                    <th scope="col">S/R</th>
<th scope="col">|</th>
                    <th scope="col">O</th>
                    <th scope="col">M</th>
                    <th scope="col">R</th>
                    <th scope="col">W</th>
                    <th scope="col">Econ</th>
     
                  </tr>
                </thead>
                <tbody>';
                foreach($players as $pl){
                    $html .=  '<tr>
                                <td ><b>'. (isset($pl['name'])?$pl['name']:0) .'</b></td>
                                <td>'.($pl['runs']??0).'</td>
                                <td>'.($pl['balls_faced']??0).'</td>
                                <td>'.($pl['fours']??0).'</td>
                                <td>'.($pl['sixes']??0).'</td>
                                <td>'.($pl['strike_rate']??0).'</td>
                                    
<td>|</td>
                                    
                                <td>'.($pl['overs']??0).'</td>
                                <td>'.($pl['maidens']??0).'</td>
                                <td>'.($pl['runs_conceded']??0).'</td>
                                <td>'.($pl['wickets']??0).'</td>
                                <td>'.($pl['econ']??0).'</td>                                    

                              </tr>';
                }
                $html .='</tbody>
            </table> </div> <br/><br/>';
             $html .=' </div> <br/><br/><hr/><hr/><br/><br/> </body></html>';
            echo $html;
            
            
            
    }
    
}
