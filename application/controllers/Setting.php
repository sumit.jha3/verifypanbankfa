<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH."core/Checkuserlogin_Controller.php");

class Setting extends Checkuserlogin_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Settings_model');
        $this->load->model('ImmediateUserWithdrawal_model');
    }
    
    public function index()
    {
        $settings = $this->Settings_model->getAllSettings();
        $this->load->view('settings', ['settings' => $settings]);
    }
    
    public function save(){
        if(empty($this->input->post('id')) || !in_array($this->input->post('status'), [0,1]) || ($this->input->post('setting_value')=='')){
            echo  json_encode(['error' => 'Invalid Request.', 'success' => '']);
            return false;
        }
        
        $this->Settings_model->updateDocument(['status' => $this->input->post('status'), 'setting_value' => $this->input->post('setting_value')], ['id' => $this->input->post('id')]);
        
        echo  json_encode(['error' => '', 'success' => 'Setting Successfully Saved.']);
        return true;
    }
}
