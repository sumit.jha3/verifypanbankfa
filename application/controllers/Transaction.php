<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once(APPPATH."core/Checkuserlogin_Controller.php");

class Transaction extends Checkuserlogin_Controller {

    protected $time_after_accept = 45; // time in minutes
    public function __construct() {
        parent::__construct();
        $this->load->helper('cashfree_helper');
        $this->load->model('Withdrawal_model');
        $this->load->model('ImmediateUserWithdrawal_model');
    }
    
    public function withdrawalList()
    {
        $status = $this->input->get('status');
        if(!in_array($status, ['success', 'pending', 'error'])){
            $status = '';
        }
        $records = $this->Withdrawal_model->getAllRecords(1, 200, $status);
        $this->load->view('withdrawal', ['records' => $records, 'status' => $status]);
    }
    
    
    public function pendingWithdrawalProcessingFA(){
        if(empty($this->input->post('process')) || $this->input->post('amount_limit') < 1)
        {
            echo json_encode(['error' => "Invalid Request"]);
            return false;
        }
        pendingWithdrawalProcessingHelper('all', $this->input->post('amount_limit'));
    }
    
}
