<?php

require_once(APPPATH."libraries/EntitySportsTrait.php");

class EntitySportsTools extends CI_Controller {

    use EntitySportsTrait;
        
    function __construct() {
        parent::__construct();
        if(!is_cli())
            die('This can be access from Command line.');
    }
    
    function squad(){
        $this->completeMatchSquad();
    }
    
    function addCompetitionMatch(){
        $this->competitionMatches(121555);
    }
}


?>