<?php
defined('BASEPATH') OR exit('No direct script access allowed');


require_once(APPPATH."core/Checkuserlogin_Controller.php");
require_once(APPPATH."libraries/UserPanBankTrait.php");

class User extends Checkuserlogin_Controller {

    use UserPanBankTrait;
    
    public function __construct() {
        parent::__construct();
        $this->load->helper('user_pan_bank_helper');
        $this->load->model('FaUserPanBank_model');
    }
    
    public function dashboard()
    {
        // bharat : user_unique_id : 87beb6bcae

        $size = 50;
        $totalCount = $this->FaUserPanBank_model->getAllRecord(['totalCount' => true]);
        $offset =  pagination('/user/dashboard/', $size, $totalCount);
        $allSavedUserRecord = $this->FaUserPanBank_model->getAllRecord(['offset' => $offset, 'size' => $size]);
        
        $this->load->view('user_dashboard', ['user'=> $allSavedUserRecord]);
    }
    
    public function process_pan_data_from_fa() {
        if(empty($this->input->post('process')))
        {
            echo json_encode(['error' => "Invalid Request"]);
            return false;
        }
        
        $this->processPanBankDataFromFaTrait();
        echo json_encode(['error' => ""]);
        return true;
    }
    
    
    
    public function add_ifsc_code(){
        if(empty($this->input->post('ifsc_codes'))){
            echo json_encode(['error' => 'Ifsc code not empty', 'success' => '']);
            return false;
        }
        
        $ifscCodeArray = explode(",", $this->input->post('ifsc_codes'));
        $error = "";
        foreach ($ifscCodeArray as $ifscCode){
            $len = strlen($ifscCode);
            if($len != 11){
                $error .= " $ifscCode, ";
                continue;
            }
            $process = 1;
            for($i = 0; $i < $len; $i++){
                if($i < 4 && is_numeric($ifscCode[$i])){
                    $error .= " $ifscCode, "; $process = 0;
                    break;
                }else if($i == 4 && !is_numeric($ifscCode[$i])){
                    if($ifscCode[$i] == 'o'){
                        $ifscCode[$i] = 0;
                    }else{
                        $error .= " $ifscCode, "; $process = 0;
                    }
                }elseif($i > 4){
                    break;
                }
            }
            if($process){
                $rbiIfscCode = $this->db->where('ifsc_code', $ifscCode)->get('rbi_ifsc_code');
                if($rbiIfscCode->num_rows() == 0){
                    $this->db->insert('rbi_ifsc_code', ['ifsc_code' => $ifscCode]);
                }
            }
        }
        
        if(!empty($error)){
            echo json_encode(['error' => " Invalid Ifsc Code :  ".rtrim($error, ", "), 'success' => '']);
        }
        else{
            echo json_encode(['error' => '', 'success' => " Successfully Saved."]);
        }
        return true;
        
    }
        

  
    public function panBankReprocess()
    {
        $size = 500;
        $bankRecord = $this->FaUserPanBank_model->getAllRecord(['bank_reprocess' => true, 'size' => $size]);
        
        $this->load->view('user_pan_bank_reprocess', ['user'=> $bankRecord]);
    }
    
    public function panBankReprocessSubmit()
    {
        if(empty($this->input->post('ids')))
        {
            echo json_encode(['status' => False]);
            return false;
        }
        
        $ids = $this->input->post('ids');
        

        // ---------- soft delete record from Local DB -----------
        if(!empty($this->input->post('deleted'))){
            $this->deleteUserPanBankRecord($ids);
            echo json_encode(['status' => True]);
            return true;
        }
        
        if(!empty($this->input->post('rejected')) && empty($this->input->post('message'))){
            echo json_encode(['status' => False]);
            return false;
        }
        $otherData = [];
        if(!empty($this->input->post('rejected')) && !empty($this->input->post('message'))){
            $otherData = ['rejected' => 1, 'message' => $this->input->post('message')];
        }
        
        
        $status = $this->reprocessSubmitBankData($ids, $otherData);
        echo json_encode(['status' => $status]);
        return true;
    }
    
    public function trackContestUserList(){
       $this->load->model('ContestTrackUserList_model');
       $users = $this->ContestTrackUserList_model->getUsers();
       
       $this->load->view('trackContestUserList',['users' => $users]);
    }
    
    public function SaveTrackContestUser() {
        $data = $this->input->post();
        if(empty($data) || empty($data['type'])){
            echo json_encode(['status' => 0]);
            return false;
        }
        
        $this->load->model('ContestTrackUserList_model');
        if($data['type'] == 'insert'){
            if(empty($data['user_id']) || empty($data['name']))
                return false;
            
            $this->ContestTrackUserList_model->insert(['user_id' => $data['user_id'], 'name' => $data['name']]);
        }else if($data['type'] == 'update'){
            if(empty($data['id']) || !isset($data['status'])){
                echo json_encode(['status' => 0]);
            }
            $this->ContestTrackUserList_model->updateDocument(['status' => $data['status']], ['id' => $data['id']]);
        }
        echo json_encode(['status' => 1]);
        return true;
    }
    
}
