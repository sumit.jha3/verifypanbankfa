<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['fa_session_id'] = '78f68c80409aabeaddcc9da5ab25b204';
//$config['fa_session_id'] = '3b26b1a4b4723e53b4938b9f5ff793e1';
$config['hyper_verge'] = [
    'base_url' => "https://ind-verify.hyperverge.co/api",
//    'header' => ['content-type:application/json', 'appId:e08f09', 'appKey:6daa5a5c9b73a0afdafe'],   // testing credential
    'header' => ['content-type:application/json', 'appId:d20b9d', 'appKey:effc5cc3004cfc0d908b'],   // Live credential
    'verifyPAN' => '/verifyPAN',
    'getNameFromPAN' => '/getNameFromPAN',
    'checkBankAccount' => '/checkBankAccount',
    'matchFields' => '/matchFields',
    
];


$config['cashfree'] = [
    'host' => ['development' => 'https://payout-gamma.cashfree.com', 'production' => 'https://payout-api.cashfree.com'],
    'authorizeApi' => 'authorize',
    'getBeneficiaryApi' => 'getBeneficiary',
    'addBeneficiaryApi' => 'addBeneficiary',
    'requestTransferApi' => 'requestTransfer',
    'header' => [ 'X-Client-Id: CF88298C474P5CPRI0BTG2EAUQ0', 'X-Client-Secret:9b39e13eda7dfb6fb5d656f787ec3f5fe2646ae3', 'cache-control: no-cache'], // development
    'header_production' => [ 'X-Client-Id: CF104345C4D04PGOJTIUOAQ4NUFG', 'X-Client-Secret:37f4e0419a2fa221008add2d4aa31ed7eaaeaeee', 'cache-control: no-cache'], // development
    'common_withdrawal_time' => 45 // in mintues
];

$config['entitysport'] = [
    'host' => 'https://rest.entitysport.com/v2/', 
    'token' => [
//        'development' => 'ec471071441bb2ac538a0ff901abd249',
        'development' => '8b8d0ea4ace9f4474c76b6243c9529c3',
        'production' => '8b8d0ea4ace9f4474c76b6243c9529c3'
        ],                
    ];
        